'use strict';

exports.handler = function (event, context, callback) {

    let listOfIntents = {
        "Amenities Request": "AM",
        "Housekeeping Request": "HK",
        "Engineering Request": "EN",
        "Laundry Request": "LA",
        "Food Order": "FO"
    }

    let Result = {
        Success: true,
        data: {
            Intents: listOfIntents,
            currentTimestamp: new Date()
        }
    };

    callback(null, { "body": JSON.stringify(Result) });
}
