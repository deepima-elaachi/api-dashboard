'use strict';

var AWS = require("aws-sdk");

AWS.config.update({
    region: "us-east-1",
    endpoint: "dynamodb.us-east-1.amazonaws.com	",
    "secretAccessKey": "NXt3qlQOW0kbPFT/Iani0pYyN2gVbkBcT37WXHAd",
    "accessKeyId": "AKIAUDHBUE6USG6RB736"
});

var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = function (event, context, callback) {

    const events = JSON.parse(event.body);
    let params = {
        TableName: events.hotelId,
        Key: {
            "ItemType": "UserCreds",
            "ItemName": events.userName
        }
    };

    docClient.get(params, fetchUserPermissions);

    function fetchUserPermissions(err, data) {
        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
            let Result = {
                Success: false,
                Error: { Message: "Database query error" }
            }
            callback(null, { "body": JSON.stringify(Result) });

        } else {
            console.log("Get Item");
            if (data.Item) {
                let Permissions = data.Item.Permissions;

                let params1 = {
                    TableName: events.hotelId,
                    KeyConditionExpression: "#ItemType = :itemType",
                    ExpressionAttributeNames: {
                        "#ItemType": "ItemType"
                    },
                    ExpressionAttributeValues: {
                        ":itemType": "RequestId"
                    }
                };

                docClient.query(params1, fetchRequests);

                function fetchRequests(err1, data1) {
                    if (err1) {
                        console.error("Unable to query. Error:", JSON.stringify(err1, null, 2));
                        let Result = {
                            Success: false,
                            Error: { Message: "Database query error" }
                        }
                        callback(null, { "body": JSON.stringify(Result) });

                    }
                    else {
                        let data11 = [];
                        if (events.previousTimestamp == "") {
                            data11 = data1.Items;
                            let counter = 0;
                            let listRequest = [];
                            while (counter < (Permissions.length)) {
                                for (let i = 0; i < data11.length; i++) {
                                    if (Math.abs((new Date().getTime()) - (new Date(data11[i].timestamp)).getTime()) <= 172800000) {

                                        console.log("Time is" + Math.abs((new Date().getTime()) - (new Date(data11[i].timestamp)).getTime()));

                                        if (data11[i].ItemName.substr(0, 2) == Permissions[counter]) {
                                            if (data11[i].ItemName.substr(0, 2) === "AM" || data11[i].ItemName.substr(0, 2) === "SR" || data11[i].ItemName.substr(0, 2) === "LA") {
                                                var requestTitle = "Housekeeping";
                                                var category = `"AM","SR","LA"`;
                                            }
                                            else if (data11[i].ItemName.substr(0, 2) === "FO") {
                                                var requestTitle = "Food Order";
                                                var category = "FO";
                                            }
                                            else if (data11[i].ItemName.substr(0, 2) === "VL") {
                                                var requestTitle = "Valet";
                                                var category = "VL";
                                            }
                                            else if (data11[i].ItemName.substr(0, 2) === "BS") {
                                                var requestTitle = "Spa Reservation";
                                                var category = "BS";
                                            }
                                            else if (data11[i].ItemName.substr(0, 2) === "BT") {
                                                var requestTitle = "Table Booking";
                                                var category = "BT";
                                            }
                                            else if (data11[i].ItemName.substr(0, 2) === "CO") {
                                                var requestTitle = "Checkout";
                                                var category = `"CO","ECO"`;
                                            }
                                            else if (data11[i].ItemName.substr(0, 3) === "ECO") {
                                                var requestTitle = "Extended Checkout";
                                                var category = `"CO","ECO"`;

                                            }

                                            let orderId = data11[i].requests.requestId.split('/');
                                            orderId = orderId[0] + orderId[1] + orderId[4];
                                            let requestObj = {
                                                "orderId": orderId,
                                                "requestId": data11[i].requests.requestId,
                                                "RoomNumber": data11[i].requests.roomNumber,
                                                "Status": (data11[i].status === "Completed") ? 1003 : ((data11[i].status === "Pending") ? 1001 : 1002),
                                                "Slots": data11[i].requests.slotValues,
                                                "timestamp": new Date(data11[i].timestamp + ' UTC').getTime(),
                                                "hotelName": events.hotelId,
                                                "requestType": data11[i].ItemName.substr(0, 2),
                                                "requestTitle": requestTitle,
                                                "ETA": (data11[i].requests.ETA == "undefined") ? 0 : parseInt(data11[i].requests.ETA.split(' ')[0]),
                                                "UpdatedETATimestamp": (data11[i].requests.ETAUpdateTimestamp) ? new Date(new Date(data11[i].requests.ETAUpdateTimestamp) + ' UTC').getTime() : 0,
                                                "category": category

                                            }

                                            listRequest.push(requestObj);
                                        }
                                    }

                                }
                                counter++;
                            }
                            let Result = {
                                Success: true,
                                Result: {
                                    Requests: listRequest,
                                    currentTimestamp: new Date()
                                }
                            };
                            callback(null, { "body": JSON.stringify(Result) });


                        } else {

                            console.log("Permissions get");
                            for (let i = 0; i < data1.Items.length; i++) {

                                if (((new Date(events.previousTimestamp)).getTime() <= (new Date(data1.Items[i].timestamp)).getTime()) || ((new Date(events.previousTimestamp)).getTime() <= (new Date(data1.Items[i].requests.ETAUpdateTimestamp)).getTime())) {
                                    data11.push(data1.Items[i]);
                                }
                            }
                            console.log("length of data11 is" + data11.length);

                            if (data11.length != 0) {

                                let counter = 0;
                                let listRequest = [];
                                while (counter < (Permissions.length)) {
                                    for (let i = 0; i < data11.length; i++) {

                                        console.log("data11[i] is" + data11[i]);

                                        if (data11[i].ItemName.substr(0, 2) == Permissions[counter]) {
                                            if (data11[i].ItemName.substr(0, 2) === "AM" || data11[i].ItemName.substr(0, 2) === "SR" || data11[i].ItemName.substr(0, 2) === "LA") {
                                                var requestTitle = "Housekeeping";
                                                var category = `"AM","SR","LA"`;
                                            }
                                            else if (data11[i].ItemName.substr(0, 2) === "FO") {
                                                var requestTitle = "Food Order";
                                                var category = "FO";
                                            }
                                            else if (data11[i].ItemName.substr(0, 2) === "VL") {
                                                var requestTitle = "Valet";
                                                var category = "VL";
                                            }
                                            else if (data11[i].ItemName.substr(0, 2) === "BS") {
                                                var requestTitle = "Spa Reservation";
                                                var category = "BS";
                                            }
                                            else if (data11[i].ItemName.substr(0, 2) === "BT") {
                                                var requestTitle = "Table Booking";
                                                var category = "BT";
                                            }
                                            else if (data11[i].ItemName.substr(0, 2) === "CO") {
                                                var requestTitle = "Checkout";
                                                var category = `"CO","ECO"`;
                                            }
                                            else if (data11[i].ItemName.substr(0, 3) === "ECO") {
                                                var requestTitle = "Extended Checkout";
                                                var category = `"CO","ECO"`;

                                            }

                                            let orderId = data11[i].requests.requestId.split('/');
                                            orderId = orderId[0] + orderId[1] + orderId[4];
                                            let requestObj = {
                                                "orderId": orderId,
                                                "requestId": data11[i].requests.requestId,
                                                "RoomNumber": data11[i].requests.roomNumber,
                                                "Status": (data11[i].status === "Approved") ? 1003 : ((data11[i].status === "Pending") ? 1001 : 1002),
                                                "Slots": data11[i].requests.slotValues,
                                                "timestamp": new Date(data11[i].timestamp + ' UTC').getTime(),
                                                "hotelName": events.hotelId,
                                                "requestType": data11[i].ItemName.substr(0, 2),
                                                "requestTitle": requestTitle,
                                                "ETA": (data11[i].requests.ETA == "undefined") ? 0 : parseInt(data11[i].requests.ETA.split(' ')[0]),
                                                "UpdatedETATimestamp": (data11[i].requests.ETAUpdateTimestamp) ? new Date(new Date(data11[i].requests.ETAUpdateTimestamp) + ' UTC').getTime() : 0,
                                                "category": category
                                            };

                                            listRequest.push(requestObj);
                                        }
                                    }
                                    counter++;
                                }
                                let Result = {
                                    Success: true,
                                    Result: {
                                        Requests: listRequest,
                                        currentTimestamp: new Date()
                                    }
                                };
                                callback(null, { "body": JSON.stringify(Result) });
                            } else {
                                let Result = {
                                    Success: true,
                                    Result: {
                                        Requests: [],
                                        currentTimestamp: new Date()
                                    }
                                };
                                callback(null, { "body": JSON.stringify(Result) });
                            }

                        }

                    }
                }
            } else {
                let Result = {
                    Success: false,
                    Error: { Message: "User name does not exist" }
                };
                callback(null, { "body": JSON.stringify(Result) });
            }


        }

    }

}
