// var fs = require('fs');
var fs = require('fs');
//= require('../interaction.json');
let modelValues = fs.readFileSync('/tmp/interaction.json', 'utf8');
console.log('model', modelValues);
console.log("modelValues", JSON.stringify(modelValues));
if (modelValues && modelValues.types && Array.isArray(modelValues.types) && modelValues.types.length > 0) {
    console.log('inside');
    let Model = {
        "interactionModel": {
            "languageModel": {
                "invocationName": modelValues.invocationName,
                "intents": [
                    {
                        "name": "AMAZON.CancelIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.HelpIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.MoreIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.NavigateHomeIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.NavigateSettingsIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.NextIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.PageDownIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.PageUpIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.PreviousIntent",
                        "samples": [
                            "go back"
                        ]
                    },
                    {
                        "name": "AMAZON.ScrollDownIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.ScrollLeftIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.ScrollRightIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.ScrollUpIntent",
                        "samples": []
                    },
                    {
                        "name": "AMAZON.StopIntent",
                        "samples": []
                    },
                    {
                        "name": "Amenities",
                        "slots": [],
                        "samples": [
                            "Amenities",
                            "Amenity",
                            "order amenities"
                        ]
                    },
                    {
                        "name": "AmenitiesIntent",
                        "slots": [
                            {
                                "name": "Toiletries",
                                "type": "Toiletries",
                                "samples": [
                                    "{Toiletries}",
                                    "i want {Toiletries}",
                                    "i want to order {Toiletries}"
                                ]
                            },
                            {
                                "name": "Number",
                                "type": "AMAZON.NUMBER",
                                "samples": [
                                    "{Number}",
                                    "{Number} please"
                                ]
                            }
                        ],
                        "samples": [
                            "to please send up {Number} {Toiletries}",
                            "to send up {Number} {Toiletries} please",
                            "send up {Number} {Toiletries} please",
                            "please send up {Number} {Toiletries}",
                            "to send up {Number} {Toiletries}",
                            "send up {Number} {Toiletries}",
                            "yes i'd like to have {Number} {Toiletries} please",
                            "yes i'd like to have {Number} {Toiletries} ",
                            "i'd like to have {Number} {Toiletries} please",
                            "i'd like to have {Number} {Toiletries}",
                            "yes i'd like to order {Number} {Toiletries} please",
                            "yes i'd like to order {Number} {Toiletries}",
                            "i'd like to order {Number} {Toiletries} please",
                            "i'd like to order {Number} {Toiletries} ",
                            "yes i'd like to order {Toiletries} please",
                            "yes i'd like to order {Toiletries}",
                            "i'd like to order {Toiletries} please",
                            "i'd like to order {Toiletries} ",
                            "for an extra {Toiletries}",
                            "to order extra {Toiletries} please",
                            "to please order extra {Toiletries}",
                            "order extra {Toiletries} please",
                            "please order extra {Toiletries}",
                            "order extra {Toiletries}",
                            "to order extra {Toiletries}",
                            "could you please get me {Number} {Toiletries} as well",
                            "yes please get me {Number} {Toiletries}",
                            "yes please get me {Number} {Toiletries} as well",
                            "yes {Number} {Toiletries} as well",
                            "{Number} {Toiletries} as well",
                            "to get me {Toiletries}",
                            "get me {Number} {Toiletries} as well",
                            "please get me {Number} {Toiletries} as well",
                            "can you please get me {Number} extra {Toiletries}",
                            "can i get {Number} extra {Toiletries}",
                            "can i get {Number} extra {Toiletries} please",
                            "please get me {Number} extra {Toiletries}",
                            "can you get me {Number} extra {Toiletries}",
                            "i would like to have {Number} extra {Toiletries}",
                            "i need {Number} extra {Toiletries}",
                            "{Number} extra {Toiletries}",
                            "to send me {Number} {Toiletries} please",
                            "to please send me {Number} {Toiletries}",
                            "to send me {Number} {Toiletries}",
                            "send me {Number} {Toiletries} please",
                            "please send me {Number} {Toiletries}",
                            "send me {Number} {Toiletries}",
                            "These is no {Toiletries} here. please send",
                            "I want {Number} extra {Toiletries}",
                            "can you get me {Number} {Toiletries} please",
                            "can you get me {Number} {Toiletries}",
                            "to send me {Toiletries} please",
                            "to please send me {Toiletries}",
                            "send me {Toiletries} please",
                            "please send me {Toiletries}",
                            "to send me {Toiletries}",
                            "send me {Toiletries}",
                            "yes extra {Toiletries} please",
                            "yes extra {Toiletries}",
                            "{Number} extra {Toiletries} please",
                            "{Number}  extra {Toiletries}",
                            "to please send me a {Toiletries}",
                            "to send me a {Toiletries} please",
                            "please send me a {Toiletries}",
                            "send me a {Toiletries} please",
                            "to send me a {Toiletries}",
                            "send me a {Toiletries}",
                            "to bring some extra {Toiletries} please",
                            "to please bring some extra {Toiletries}",
                            "to bring some extra {Toiletries}",
                            "bring some extra {Toiletries} please",
                            "please bring some extra {Toiletries}",
                            "bring some extra {Toiletries} ",
                            "to send some extra {Toiletries} please",
                            "to please send some extra {Toiletries}",
                            "send some extra {Toiletries} please",
                            "please send some extra {Toiletries}",
                            "to send some extra {Toiletries}",
                            "send some extra {Toiletries}",
                            "yeah send {Number} extra {Toiletries}",
                            "yes send {Number} extra {Toiletries} please",
                            "yes send {Number} extra {Toiletries}",
                            "to please send {Number} extra {Toiletries}",
                            "to send {Number} extra {Toiletries} please",
                            "to send {Number} extra {Toiletries}",
                            "send {Number} extra {Toiletries} please",
                            "can i get {Number} {Toiletries} please",
                            "send {Number} extra {Toiletries}",
                            "please send {Number} extra {Toiletries}",
                            "can i get {Number} {Toiletries}",
                            "{Toiletries}",
                            "Yes a {Toiletries} please",
                            "to send extra {Toiletries}",
                            "get me {Number} {Toiletries} please",
                            "order me {Number} {Toiletries}",
                            "order {Number} {Toiletries}",
                            "can you order {Number} {Toiletries}",
                            "i want to order {Number} {Toiletries}",
                            "get me {Toiletries}",
                            "get me {Number} {Toiletries}",
                            "hand me {Number} {Toiletries}",
                            "Hand me some {Toiletries}",
                            "{Toiletries} please",
                            "{Number} {Toiletries} please",
                            "could i get {Number} {Toiletries}",
                            "could i get some {Toiletries}",
                            "to send in {Number} {Toiletries}",
                            "to send in a few {Toiletries}",
                            "could you send me some {Toiletries}",
                            "to get me {Number} {Toiletries}",
                            "send me few {Toiletries}",
                            "to order {Number} extra {Toiletries}",
                            "yes i need {Toiletries} please",
                            "yes i need {Toiletries}",
                            "yes i need {Number} {Toiletries} please",
                            "yes i need {Number} {Toiletries}",
                            "yes get me {Toiletries} please",
                            "yes get me {Toiletries}",
                            "yes get me {Number} {Toiletries} please",
                            "yes get me {Number} {Toiletries}",
                            "yes send in {Toiletries} please",
                            "yes send in {Toiletries}",
                            "yes send in {Number} {Toiletries} please",
                            "yes send in {Number} {Toiletries}",
                            "yes can i get {Toiletries} please",
                            "yes can i get {Toiletries}",
                            "yes can i get {Number} {Toiletries} please",
                            "yes can i get {Number} {Toiletries}",
                            "yes i would like to order {Number} {Toiletries} please",
                            "yes i would like to order {Number} {Toiletries}",
                            "yes i would like to order {Toiletries} please",
                            "yes i would like to order {Toiletries}",
                            "yes i would like to have {Toiletries} please",
                            "yes i want {Toiletries} please",
                            "yes i want {Toiletries}",
                            "yes i would like to have {Toiletries}",
                            "yes i would like to have {Number} {Toiletries}",
                            "yes i want {Number} {Toiletries} please",
                            "yes i want {Number} {Toiletries}",
                            "I want {Number}  {Toiletries} please",
                            "I want {Toiletries} please",
                            "{Number} {Toiletries}",
                            "I would like to have {Number} {Toiletries} please",
                            "I would like to have {Number} {Toiletries}",
                            " I want {Toiletries}",
                            " I want {Number} {Toiletries} please",
                            "i want {Number} {Toiletries}",
                            "to bring {Toiletries}",
                            "to bring {Toiletries} please",
                            "to bring {Toiletries} for me",
                            "to bring {Toiletries} for me please",
                            "to bring {Number} {Toiletries}",
                            "to bring {Number} {Toiletries} please",
                            "i need {Toiletries}",
                            "i need {Toiletries} please",
                            "bring me {Toiletries}",
                            "send in a {Toiletries}",
                            "bring me {Number} {Toiletries}",
                            "bring me {Number} {Toiletries} please",
                            "to bring me {Number} {Toiletries}",
                            "to bring me {Number} {Toiletries} please",
                            "to please bring me {Number} {Toiletries}",
                            "for extra {Toiletries}",
                            "for {Number} extra {Toiletries}",
                            "yes {Number} {Toiletries}",
                            "yes {Number} {Toiletries} please",
                            "yes {Toiletries}",
                            "yes {Toiletries} please",
                            "for extra {Number} {Toiletries}",
                            "extra {Toiletries}",
                            "i need extra {Toiletries}",
                            "i need {Number} {Toiletries}",
                            "bring me few {Toiletries}",
                            "get me few {Toiletries}",
                            "can you get me few {Toiletries}",
                            "order {Toiletries}",
                            "{Number} {Toiletries} too",
                            "order me {Toiletries}",
                            "send in {Number} extra {Toiletries}",
                            "send in {Number} extra {Toiletries} please",
                            "send in {Toiletries}",
                            "send in {Toiletries} please",
                            "send in {Toiletries} for me",
                            "send in {Toiletries} for me please",
                            "send in a {Toiletries} please",
                            "send in {Number} {Toiletries}",
                            "send in {Number} {Toiletries} please",
                            "please bring {Number} extra {Toiletries}",
                            "please bring {Number} extra {Toiletries} please",
                            "please bring {Toiletries}",
                            "please bring {Toiletries} please",
                            "please bring {Toiletries} for me",
                            "please bring {Toiletries} for me please",
                            "please bring a {Toiletries}",
                            "please bring a {Toiletries} please",
                            "please bring {Number} {Toiletries}",
                            "please bring {Number} {Toiletries} please",
                            "please bring me {Number} extra {Toiletries}",
                            "please bring me {Number} extra {Toiletries} please",
                            "please bring me {Toiletries}",
                            "please bring me {Toiletries} please",
                            "please bring me {Toiletries} for me",
                            "please bring me {Toiletries} for me please",
                            "please bring me a {Toiletries}",
                            "please bring me a {Toiletries} please",
                            "please send in {Toiletries}",
                            "please send in {Toiletries} please",
                            "please send in {Toiletries} for me",
                            "please send in {Toiletries} for me please",
                            "please send in a {Toiletries}",
                            "please send in a {Toiletries} please",
                            "please send in extra {Toiletries}",
                            "please send in extra {Toiletries} please",
                            "please send in extra {Toiletries} for me",
                            "please send in extra {Toiletries} for me please",
                            "please send in a extra {Toiletries}",
                            "please send in a extra {Toiletries} please",
                            "i need extra {Toiletries} please",
                            "i need extra {Toiletries} for me",
                            "i need extra {Toiletries} for me please",
                            "i need an extra {Toiletries}",
                            "i need an extra {Toiletries} please",
                            "get me extra {Toiletries}",
                            "get me extra {Toiletries} please",
                            "get me extra {Toiletries} for me",
                            "get me extra {Toiletries} for me please",
                            "get me an extra {Toiletries}",
                            "get me an extra {Toiletries} please",
                            "get me {Toiletries} please",
                            "get me {Toiletries} for me",
                            "get me {Toiletries} for me please",
                            "get me a {Toiletries}",
                            "get me a {Toiletries} please",
                            "can i get {Toiletries}",
                            "can i get {Toiletries} please",
                            "can i get {Toiletries} for me",
                            "can i get {Toiletries} for me please",
                            "can i get a {Toiletries}",
                            "can i get a {Toiletries} please",
                            "can i get extra {Toiletries}",
                            "can i get extra {Toiletries} please",
                            "can i get extra {Toiletries} for me",
                            "can i get extra {Toiletries} for me please",
                            "can i get a extra {Toiletries}",
                            "can i get a extra {Toiletries} please",
                            "can you get extra {Toiletries}",
                            "can you get extra {Toiletries} please",
                            "can you get extra {Toiletries} for me",
                            "can you get extra {Toiletries} for me please",
                            "can you get a extra {Toiletries}",
                            "can you get a extra {Toiletries} please",
                            "can you get {Toiletries}",
                            "can you get {Toiletries} please",
                            "can you get {Toiletries} for me",
                            "can you get {Toiletries} for me please",
                            "can you get a {Toiletries}",
                            "can you get a {Toiletries} please",
                            "can you get me {Toiletries}",
                            "can you get me {Toiletries} please",
                            "can you get me {Toiletries} for me",
                            "can you get me {Toiletries} for me please",
                            "can you get me a {Toiletries}",
                            "can you get me a {Toiletries} please",
                            "can you get me extra {Toiletries}",
                            "can you get me extra {Toiletries} please",
                            "can you get me extra {Toiletries} for me",
                            "can you get me extra {Toiletries} for me please",
                            "can you get me a extra {Toiletries}",
                            "can you get me a extra {Toiletries} please",
                            "can you please get me extra {Toiletries}",
                            "can you please get me extra {Toiletries} please",
                            "can you please get me extra {Toiletries} for me",
                            "can you please get me extra {Toiletries} for me please",
                            "can you please get me a extra {Toiletries}",
                            "can you please get me a extra {Toiletries} please",
                            "can you please get me {Toiletries}",
                            "can you please get me {Toiletries} please",
                            "can you please get me {Toiletries} for me",
                            "can you please get me {Toiletries} for me please",
                            "can you please get me a {Toiletries}",
                            "can you please get me a {Toiletries} please",
                            "order me {Toiletries} please",
                            "order me {Toiletries} for me",
                            "order me {Toiletries} for me please",
                            "order me a {Toiletries}",
                            "order me a {Toiletries} please",
                            "order {Toiletries} for me",
                            "order {Toiletries} for me please",
                            "order a {Toiletries}",
                            "order a {Toiletries} please",
                            "i want to order extra {Toiletries}",
                            "i want to order extra {Toiletries} please",
                            "i want to order extra {Toiletries} for me",
                            "i want to order extra {Toiletries} for me please",
                            "i want to order an extra {Toiletries}",
                            "i want to order an extra {Toiletries} please",
                            "i want to order {Toiletries}",
                            "i want to order {Toiletries} please",
                            "i want to order {Toiletries} for me",
                            "i want to order {Toiletries} for me please",
                            "i want to order a {Toiletries}",
                            "i want to order a {Toiletries} please",
                            "i would like to order {Number} {Toiletries}"
                        ]
                    },
                    {
                        "name": "AvailabilityIntent",
                        "slots": [
                            {
                                "name": "Toiletries",
                                "type": "Toiletries"
                            },
                            {
                                "name": "housekeeping",
                                "type": "Housekeeping"
                            }
                        ],
                        "samples": [
                            "is  {housekeeping} service available in this hotel",
                            "is {housekeeping} available in this hotel",
                            "is {Toiletries} available in this hotel",
                            "is {Toiletries} available"
                        ]
                    },
                    {
                        "name": "BellBoyIntent",
                        "slots": [
                            {
                                "name": "luggage",
                                "type": "AMAZON.NUMBER",
                                "samples": [
                                    "{luggage}"
                                ]
                            }
                        ],
                        "samples": [
                            "yes send belldesk to pick up my luggage please",
                            "yes please send belldesk to pick up my luggage",
                            "yes send belldesk to pick up my luggage",
                            "to send belldesk to pick up my luggage please",
                            "to please send belldesk to pick up my luggage",
                            "send belldesk to pick up my luggage please",
                            "please send belldesk to pick up my luggage",
                            "send belldesk to pick up my luggage",
                            "to send belldesk to pick up my luggage",
                            "please ask the belldesk to send for a bellboy to get my luggage picked up",
                            "ask the belldesk to send for a bellboy to get my luggage picked up",
                            "please ask the belldesk to send a bellboy to get my luggage picked up",
                            "ask the belldesk to send a bellboy to get my luggage picked up",
                            "please ask the belldesk to send for a bellboy to pick up my luggage",
                            "ask the belldesk to send for a bellboy to pick up my luggage",
                            "please ask the belldesk to send a bellboy to pick up my luggage",
                            "ask the belldesk to send a bellboy to pick up my luggage",
                            "please ask the belldesk to send for a bellboy",
                            "please ask the belldesk to send a bellboy",
                            "ask the belldesk to send for a bellboy",
                            "ask the belldesk to send a bellboy",
                            "please ask the belldesk to get my luggage picked up at once",
                            "please ask the belldesk to get my luggage picked up",
                            "ask the belldesk to get my luggage picked up at once",
                            "ask the belldesk to get my luggage picked up",
                            "please ask the belldesk to pick up my luggage",
                            "please ask the belldesk to pick up my luggage at once",
                            "ask the belldesk to pick up my luggage at once",
                            "ask the belldesk to pick up my luggage",
                            "please send a bellboy to pick up my luggage at once",
                            "please send for a bellboy to pick up my luggage",
                            "send for a bellboy to pick up my luggage at once",
                            "send for a bellboy to pick up my luggage",
                            "send a bellboy to pick up my luggage at once",
                            "send a bellboy to pick up my luggage",
                            "please send for a bellboy at once",
                            "please send a bellboy at once",
                            "please send for a bellboy",
                            "please send a bellboy",
                            "send for a bellboy at once",
                            "send a bellboy at once",
                            "send for a bellboy",
                            "send a bellboy",
                            "please send the bellboy to pick up my luggage",
                            "please send bellboy to pick up my luggage",
                            "yes please send belldesk",
                            "yes send belldesk please",
                            "yes send belldesk",
                            "yes send bellboy please",
                            "yes please send bellboy",
                            "yes send bellboy",
                            "to send bellboy please",
                            "to please send bellboy",
                            "send bellboy please",
                            "please send bellboy",
                            "to send bellboy",
                            "to send belldesk please",
                            "to please send belldesk",
                            "send belldesk please",
                            "please send belldesk",
                            "to send belldesk",
                            "send bellboy",
                            "send belldesk",
                            "yes please send a bell desk",
                            "yes send a bell desk please",
                            "yes send a bell desk",
                            "to send a bell desk please",
                            "to please send a bell desk",
                            "to send a bell desk",
                            "send a bell desk please",
                            "please send a bell desk",
                            "to send a bell desk to pick up my luggage please",
                            "to please send a bell desk to pick up my luggage",
                            "to send a bell desk to pick up my luggage",
                            "send a bell desk to pick up my luggage please",
                            "please send a bell desk to pick up my luggage",
                            "send a bell desk to help me",
                            "send a bell desk",
                            "send a bell desk to pick up my luggage",
                            "can you please send someone to help me with my baggage",
                            "can you please send someone to help me with my bags",
                            "could you please send someone to help me with my bags",
                            "could you please send someone to help me with my luggage",
                            "can you please send someone to help me with my luggage",
                            "send a bell boy please",
                            "can you please send a bell boy",
                            "please send a bell boy",
                            "send someone to help me with my baggage",
                            "send someone to help me with my luggage",
                            "send a bell boy",
                            "to send a bell boy to help me with the luggage",
                            "send a bell boy to help me with the luggage",
                            "please send a bell boy to help me with my luggage",
                            "send a bell boy to help me with my luggage please",
                            "send a bell boy to help me with my luggage",
                            "to send a bell boy to help me with my luggage",
                            "i need a bell boy bell boy bell boy bell boy bell boy bell boy bell boy bell boy bell boy bell boy bell boy bell boy bell boy bell boy {luggage}",
                            "i need a bell boy to help me with my luggage",
                            "i need a bell boy",
                            "send someone to help me out with my luggage",
                            "i need bell boy",
                            "call the bell boy",
                            "send someone to pick up my luggage",
                            "send someone to pick up my luggage please",
                            "please send someone to pick up my luggage",
                            "to send someone to pick up my luggage",
                            "to send someone to pick up my luggage please",
                            "to please send someone to pick up my luggage",
                            "send someone to pick up my bags",
                            "send someone to pick up my bags please",
                            "please send someone to pick up my bags",
                            "to send someone to pick up my bags please",
                            "to send someone to pick up my bags",
                            "to please send someone to pick up my bags",
                            "can you please send someone to pick up my luggage",
                            "can you please send someone to pick up my luggage from room",
                            "can you please send someone to pick up my bags",
                            "can you please send someone to pick up my bags from room",
                            "send someone to get my luggage",
                            "send someone to get my luggage please",
                            "please send someone to get my luggage",
                            "to send someone to get my luggage",
                            "to send someone to get my luggage please",
                            "to please send someone to get my luggage",
                            "send someone to get my bags",
                            "send someone to get my bags please",
                            "please send someone to get my bags",
                            "to send someone to get my bags please",
                            "to send someone to get my bags",
                            "to please send someone to get my bags",
                            "send someone to pick up my bag",
                            "please send someone to pick up my bag",
                            "send someone to pick up my bag please",
                            "to send someone to pick up my bag",
                            "to send someone to pick up my bag please",
                            "to please send someone to pick up my bag",
                            "send someone to get my bag",
                            "send someone to get my bag please",
                            "please send someone to get my bag",
                            "to send someone to get my bag",
                            "to send someone to get my bag please",
                            "to please send someone to get my bag"
                        ]
                    },
                    {
                        "name": "BookSpa",
                        "slots": [],
                        "samples": []
                    },
                    {
                        "name": "BookSpaIntent",
                        "slots": [
                            {
                                "name": "Date",
                                "type": "AMAZON.DATE",
                                "samples": [
                                    "{Date}",
                                    "on {Date}",
                                    "for {Date}"
                                ]
                            },
                            {
                                "name": "Time",
                                "type": "AMAZON.TIME",
                                "samples": [
                                    "at {Time}",
                                    "{Time}"
                                ]
                            },
                            {
                                "name": "Number",
                                "type": "AMAZON.NUMBER",
                                "samples": [
                                    "{Number}",
                                    "for {Number}",
                                    "for {Number} people"
                                ]
                            }
                        ],
                        "samples": [
                            "yes book a spa slot at {Time} {Date} please",
                            "yes please book a spa slot at {Time} {Date}",
                            "yes book a spa slot at {Time} {Date}",
                            "to book a spa slot at {Time} {Date} please",
                            "to please book a spa slot at {Time} {Date}",
                            "book a spa slot at {Time} {Date} please",
                            "please book a spa slot at {Time} {Date}",
                            "to book a spa slot at {Time} {Date}",
                            "book a spa slot at {Time} {Date}",
                            "book spa slot for me",
                            "book a spa slot for me",
                            "to reserve a spa slot for me please",
                            "to reserve a spa slot for me",
                            "to reserve a spa slot",
                            "to book a spa slot for me please",
                            "to book a spa slot for me",
                            "Great. Reserve a session at {Time} {Date} please",
                            "Awesome. Reserve a session at {Time} {Date}",
                            "to Reserve a session at {Time} {Date}",
                            "Reserve a session at {Time} {Date}",
                            "Sure. Reserve a session at {Time} {Date}",
                            "Great. Reserve a session at {Time} {Date}",
                            "reservation please",
                            "reservation",
                            "book a spa slot for {Number} for {Date}",
                            "book a spa slot for {Number} at {Time}",
                            "please book a spa slot for {Number}",
                            "book a spa slot for {Number}",
                            "please book a slot for {Number} at {Time} {Date} at the spa",
                            "book a slot for {Number} at {Time} {Date} at the spa please",
                            "book a slot for {Number} at {Time} {Date} at the spa",
                            "to please book a slot for {Number} at {Time} {Date} at the spa",
                            "to book a slot for {Number} at {Time} {Date} at the spa please",
                            "to book a slot for {Number} at {Time} {Date} at the spa",
                            "i want to book a spa slot on {Date} at {Time} for {Number}",
                            "i want to book a spa slot on {Date} at {Time}",
                            "i want to book a spa slot for {Date} {Time}",
                            "i want to book a spa slot for {Number}",
                            "i want to book a spa slot",
                            "i would like to book a spa slot on {Date} at {Time} for {Number}",
                            "i would like to book a spa slot on {Date} at {Time}",
                            "i would like to book a spa slot on {Date}",
                            "i would like to book a spa slot at {Time} {Date}",
                            "i would like to book a spa slot for {Number}",
                            "i would like to book a spa slot for {Time} {Date}",
                            "i would like to book a spa slot",
                            "to book a spa slot please",
                            "to please book a spa slot",
                            "please book a spa slot",
                            "book a spa slot please",
                            "book a spa slot",
                            "to book a spa slot",
                            "book spa slot",
                            "to book spa",
                            "book spa",
                            "to book me a spa slot for {Time} {Date} please",
                            "to please book me a spa slot for {Time} {Date}",
                            "to book me a spa slot for {Time} {Date}",
                            "please book me a spa slot for {Time} {Date}",
                            "book me a spa slot for {Time} {Date} please",
                            "book me a spa slot for {Time} {Date} ",
                            "to book me a spa slot please",
                            "to please book me a spa slot",
                            "to book me a spa slot",
                            "please book me a spa slot",
                            "book me a spa slot please",
                            "book me a spa slot",
                            "book spa at {Time} {Date} for {Number}",
                            "please book spa at {Time} {Date}",
                            "book a spa at {Time} on {Date} please",
                            "book a spa at {Time} on {Date}",
                            "book a slot at spa for {Time} {Date}",
                            "book a slot at spa for {Time} {Date} please",
                            "please book a slot at spa for {Time} {Date}",
                            "to book a slot at spa for {Time} {Date}",
                            "to book a slot at spa for {Time} {Date} please",
                            "to please book a slot at spa for {Time} {Date}",
                            "to book spa for {Time} {Date}",
                            "book spa for {Time} {Date} please",
                            "please book spa for {Time} {Date}",
                            "to please book spa for {Time} {Date}",
                            "to book spa for {Time} {Date} please",
                            "book spa on {Date} at {Time}",
                            "book spa on {Date} at {Time} please",
                            "please book spa on {Date} at {Time}",
                            "to book spa on {Date} at {Time} please",
                            "to book spa on {Date} at {Time}",
                            "to please book spa on {Date} at {Time}",
                            "book spa at {Time} {Date}",
                            "book spa at {Time} {Date} please",
                            "to book spa at {Time} {Date}",
                            "to book spa at {Time} {Date} please",
                            "to please book spa at {Time} {Date}",
                            "book spa for {Number}",
                            "book a spa for {Number}",
                            "book spa for {Date} at {Time}",
                            "book a spa at {Time}",
                            "please make a spa reservation",
                            "can i make a spa reservation",
                            "can i book reservation at the spa",
                            "i want to reserve a slot at the spa tonight",
                            "can you make a reservation at spa",
                            "can you book me a spa slot",
                            "can you make a spa reservation",
                            "book spa for {Number} at {Time} {Date}",
                            "to book a slot at Spa for {Number} {Date}",
                            "to book a slot at Spa for {Number} {Date} please",
                            "to please book a slot at Spa for {Number} {Date}",
                            "to book a slot at Spa for me {Date}",
                            "to book a slot at Spa for me {Date} please",
                            "to please book a slot at Spa for me {Date}",
                            "to book a slot at Spa for {Number} {Date} at {Time}",
                            "to book a slot at Spa for {Number} {Date} at {Time} please",
                            "to please book a slot at Spa for {Number} {Date} at {Time}"
                        ]
                    },
                    {
                        "name": "BookTable",
                        "slots": [],
                        "samples": []
                    },
                    {
                        "name": "BookTableIntent",
                        "slots": [
                            {
                                "name": "Number",
                                "type": "AMAZON.NUMBER",
                                "samples": [
                                    "{Number}",
                                    "for {Number}",
                                    "for {Number} people"
                                ]
                            },
                            {
                                "name": "date",
                                "type": "AMAZON.DATE",
                                "samples": [
                                    "for {date}",
                                    "{date}",
                                    "on {date}"
                                ]
                            },
                            {
                                "name": "time",
                                "type": "AMAZON.TIME",
                                "samples": [
                                    "{time}",
                                    "at {time}"
                                ]
                            },
                            {
                                "name": "restaurant",
                                "type": "restaurant",
                                "samples": [
                                    "{restaurant}",
                                    "at {restaurant}"
                                ]
                            }
                        ],
                        "samples": [
                            "book table",
                            "book a table",
                            "to book a table for {Number}",
                            "to book a table for dinner tonight",
                            "to book a table for me",
                            "please book a table for {Number} at the {restaurant}",
                            "book a table for {Number} at the restaurant please",
                            "to please book a table for {Number} at the restaurant",
                            "to book a table for {Number} at the restaurant",
                            "make reservations for {Number} at the restaurant",
                            "make a reservation for {Number} at the restaurant",
                            "make a reservation for {Number} at the restaurant please",
                            "to make a reservation for {Number} at the restaurant please",
                            "please make a reservation for {Number} at the restaurant",
                            "to make a reservation for {Number} at the restaurant",
                            "to please make a reservation for {Number} at the restaurant ",
                            "book a table for {Number} at the restaurant",
                            "book a table for {Number} please",
                            "to book a table for {Number} please",
                            "to make reservations at the restaurant for {date} at {time}",
                            "to make reservations at the restaurant for {date} at {time} please",
                            "to please make reservations at the restaurant for {date} at {time}",
                            "to make reservations at restaurant for {date} at {time}",
                            "to make reservations at the restaurant on {date} at {time} please",
                            "to make reservations at the restaurant on {date} at {time} ",
                            "make reservations at the restaurant on {date} at {time} please",
                            "to please make reservations at the restaurant on {date} at {time} ",
                            "book a table at the restaurant for {date} at {time}",
                            "book a table at the restaurant for {date} at {time} please",
                            "to book a table at the restaurant for {date}  {time}",
                            "to please book a table at the restaurant for {date} at {time}",
                            "to book a table at the restaurant for {date}  at {time} please",
                            "please make a reservation for {Number} at the restaurant for {date} {time}",
                            "please make a reservation for {Number} at the restaurant for {date}",
                            "to make a reservation for {Number} at the restaurant for {date} {time}",
                            "to make a reservation for {Number} at the restaurant for {date} {time} please",
                            "to please make a reservation for {Number} at the restaurant for {date} {time}",
                            "book a table for {Number} people for {date} {time}",
                            "to book a table for {Number} people for {date} at {time}",
                            "book table for {Number} for {date}",
                            "please book a table for {Number} at the restaurant for {date} {time}",
                            "to book a table for {Number} at the restaurant for {date} {time}",
                            "to book a table for {Number} at the restaurant for {date} {time} please",
                            "to please book a table for {Number} at the restaurant for {date} {time}",
                            "book a table for {Number} at the restaurant for {date} {time} please",
                            "book a table for {Number} at the restaurant for {date} {time}",
                            "book a table for {date}",
                            "book a table for {time}",
                            "book a table for {Number}"
                        ]
                    },
                    {
                        "name": "channelsIntent",
                        "slots": [
                            {
                                "name": "channelList",
                                "type": "channelList"
                            }
                        ],
                        "samples": [
                            "on what channel is {channelList}"
                        ]
                    },
                    {
                        "name": "CheckOutIntent",
                        "slots": [
                            {
                                "name": "Number",
                                "type": "AMAZON.NUMBER"
                            }
                        ],
                        "samples": [
                            "yes help me checkout please",
                            "yes please help me checkout",
                            "yes help me checkout",
                            "to help me checkout please",
                            "to please help me checkout",
                            "to help me checkout",
                            "help me checkout please",
                            "please help me checkout",
                            "help me checkout",
                            "I am checking out in {Number} minutes",
                            "that I am checking out in {Number} minutes",
                            "checkout",
                            "i would like to check out",
                            "to prepare my checkout please",
                            "to please prepare my checkout",
                            "prepare my checkout please",
                            "please prepare my checkout",
                            "prepare my checkout",
                            "that i want to checkout",
                            "i want to checkout",
                            "prep for my check out",
                            "prep for my checkout",
                            "please prep my checkout",
                            "please prep my check out",
                            "to prepare my checkout",
                            "that i am checking out",
                            "that i want to check out",
                            "to prepare my check out",
                            "please prepare my check out",
                            "prepare my check out please",
                            "i am checking out",
                            "prepare my check out",
                            "i want to check out"
                        ]
                    },
                    {
                        "name": "CheckStatusIntent",
                        "slots": [
                            {
                                "name": "requeststatus",
                                "type": "requeststatus"
                            },
                            {
                                "name": "date",
                                "type": "AMAZON.DATE"
                            }
                        ],
                        "samples": [
                            "yes what is the status of my {requeststatus} ",
                            "yes what is the status of {requeststatus} ",
                            "what is the status of {requeststatus} request",
                            "what is the status of my {requeststatus} request",
                            "what is the status of {requeststatus} ",
                            "show me what {requeststatus} did i order {date}",
                            "Show me what {requeststatus} did i order on {date}",
                            "what all {requeststatus} did i order {date}",
                            "what is the status of my {requeststatus}",
                            "tell me the status of my {requeststatus}",
                            "tell me the status of my {requeststatus} please",
                            "please tell me the status of my {requeststatus}",
                            "to tell me the status of my {requeststatus}",
                            "to tell me the status of my {requeststatus} please",
                            "to please tell me the status of my {requeststatus}",
                            "tell me the status of my {requeststatus} request",
                            "tell me the status of my {requeststatus} request please",
                            "please tell me the status of my {requeststatus} request",
                            "to tell me the status of my {requeststatus} request",
                            "to tell me the status of my {requeststatus} request please",
                            "to please tell me the status of my {requeststatus} request",
                            "status of my {requeststatus} request please",
                            "status of my {requeststatus} ",
                            "status of {requeststatus} request please",
                            "What's the status of my {requeststatus} ",
                            "what about my {requeststatus} request status",
                            "what about my {requeststatus} request ",
                            "where is my {requeststatus}",
                            "where are my {requeststatus}",
                            "what is the status of my {requeststatus} reservations",
                            "tell me the status of my {requeststatus} reservations",
                            "tell me the status of my {requeststatus} reservations please",
                            "to tell me the status of my {requeststatus} reservations",
                            "to please tell me the status of my {requeststatus} reservations",
                            "to tell me the status of my {requeststatus} reservations please",
                            "to show me the status for my {requeststatus} reservation",
                            "where are my extra {requeststatus}"
                        ]
                    },
                    {
                        "name": "ConnectFrontdeskIntent",
                        "slots": [],
                        "samples": [
                            "yes connect me to reception please",
                            "yes connect me to reception",
                            "yes connect me to frontdesk please",
                            "yes connect frontdesk please",
                            "yes connect me to frontdesk",
                            "please connect frontdesk",
                            "Connect me to reception",
                            "Connect me to reception please",
                            "please connect me to reception",
                            "would you connect me to reception please",
                            "would you please connect me to reception",
                            "to connect me to reception",
                            "to connect me to reception please",
                            "to please connect me to reception",
                            "connect reception",
                            "connect reception please",
                            "please connect reception",
                            "connect frontdesk please",
                            "connect frontdesk",
                            "connect me to frontdesk",
                            "please connect me to frontdesk",
                            "to please connect me to frontdesk",
                            "to connect me to frontdesk please",
                            "call frontdesk",
                            "to call frontdesk",
                            "call frontdesk please",
                            "please call frontdesk",
                            "to please call frontdesk",
                            "to call frontdesk please",
                            "connect to frontdesk",
                            "connect me to frontdesk please",
                            "to connect me to the front desk"
                        ]
                    },
                    {
                        "name": "ConnectToReception",
                        "slots": [],
                        "samples": [
                            "reception",
                            "front desk"
                        ]
                    },
                    {
                        "name": "EventsIntent",
                        "slots": [],
                        "samples": [
                            "what's the upcoming event",
                            "what is the upcoming event",
                            "is there any upcoming event",
                            "about upcoming events please",
                            "about upcoming events",
                            "what about the upcoming events",
                            "can you please tell me about the upcoming events",
                            "yes tell me about the upcoming events please",
                            "yes please tell me about the upcoming events",
                            "yes tell me about the upcoming events",
                            "to tell me about the upcoming events please",
                            "to please tell me about the upcoming events",
                            "tell me about the upcoming events please",
                            "please tell me about the upcoming events",
                            "to tell me about the upcoming events",
                            "tell me about the upcoming events",
                            "what are the upcoming events",
                            "upcoming event",
                            "upcoming events",
                            "event in the hotel",
                            "please tell me about the events going on in the hotel",
                            "tell me about the events going on in the hotel",
                            "events going on in the hotel",
                            "about events in the hotel",
                            "about the events in the hotel",
                            "tell me about the events going on",
                            "i am bored can you tell me about the events going on",
                            "list of events",
                            "what are the events happening",
                            "what are the events happening today",
                            "what all events happening today",
                            "event",
                            "events",
                            "to show me the events list please",
                            "to please show me the events list",
                            "please show me the events list",
                            "show me the events list please",
                            "to show me the events list",
                            "show me the events list",
                            "what are the events today",
                            "to please tell me about the events",
                            "to tell me about the events please",
                            "to tell me about the events",
                            "please tell me about the events",
                            "tell me about the events please",
                            "tell me about the events",
                            "hotel events",
                            "events in the hotel",
                            "what are the events going on in the hotel",
                            "what are the events going on",
                            "what are events near by"
                        ]
                    },
                    {
                        "name": "ExtendedCheckOutIntent",
                        "slots": [
                            {
                                "name": "Time",
                                "type": "AMAZON.TIME",
                                "samples": [
                                    "just {Number} hours",
                                    "just {Number} hours please",
                                    "for {Number} hours",
                                    "{Number} hours",
                                    "just {Time}",
                                    "till {Time} please",
                                    "till {Time}",
                                    "Just {Time} please",
                                    "{Time}",
                                    "at {Time}"
                                ]
                            },
                            {
                                "name": "Number",
                                "type": "AMAZON.NUMBER"
                            }
                        ],
                        "samples": [
                            "I would like to extend my checkout by an hour",
                            "I would like to extend my stay by an hour",
                            "I would like to extend my stay by {Number} hour ",
                            "extend my checkout please",
                            "to extend my checkout",
                            "please extend my checkout",
                            "extend my checkout",
                            "extend my checkout by {Number} hours",
                            "extend my stay by {Number} hours",
                            "i want to extend my checkout by {Number} hours",
                            "i would like to extend my checkout by {Number} hours",
                            "i would like to extend my stay by {Number} hours",
                            "i would like to extend my stay by {Number}",
                            "i would like to extend my stay by {Time}",
                            "i would like to extend my stay by an {Time}",
                            "i would like to extend my checkout by an {Time}",
                            "i would like to extend my checkout by {Time}",
                            "I would like to extend my check out",
                            "that I would like to extend my check out",
                            "could i stay for longer",
                            "please extend my check out",
                            "please extend my check out to {Time}",
                            "extend my check out",
                            "extend my check out to {Time}"
                        ]
                    },
                    {
                        "name": "facilitiesIntent",
                        "slots": [
                            {
                                "name": "categoryTwo",
                                "type": "Facilities"
                            }
                        ],
                        "samples": [
                            "where do i find {categoryTwo}",
                            "where can i find {categoryTwo}",
                            "where is {categoryTwo}",
                            "is there a {categoryTwo} in the property",
                            "yes {categoryTwo} please",
                            "yes {categoryTwo}",
                            "{categoryTwo} please",
                            "what about the {categoryTwo}",
                            "what about {categoryTwo}",
                            "yes about {categoryTwo} please",
                            "yes about {categoryTwo}",
                            "yes about the {categoryTwo} please",
                            "yes about the {categoryTwo}",
                            "about the {categoryTwo} please",
                            "about the {categoryTwo}",
                            "yes tell me about the {categoryTwo} please",
                            "yes please tell me about the {categoryTwo}",
                            "yes tell me about the {categoryTwo}",
                            "to tell me about the {categoryTwo} please",
                            "to please tell me about the {categoryTwo}",
                            "to tell me about the {categoryTwo}",
                            "tell me about the {categoryTwo} please",
                            "tell me about the {categoryTwo}",
                            "please tell me about the {categoryTwo} ",
                            "about {categoryTwo} please",
                            "about {categoryTwo}",
                            "could you please tell me about the {categoryTwo} services",
                            "can you please tell me about the {categoryTwo} services",
                            "please could you tell me about the {categoryTwo} services",
                            "please can you tell me about the {categoryTwo} services",
                            "can you tell me about the {categoryTwo} services",
                            "could you tell me about the {categoryTwo} services",
                            "about the {categoryTwo} services",
                            "tell me about the {categoryTwo} services",
                            "can you please tell me about {categoryTwo} ",
                            "can you tell me about {categoryTwo}",
                            "whats happening in {categoryTwo}",
                            "please tell me about {categoryTwo}",
                            "tell me about {categoryTwo} please",
                            "what all facilities are offered in {categoryTwo}",
                            "what are the facilities available in {categoryTwo}",
                            "tell me about {categoryTwo}",
                            "what are the facilities offered in {categoryTwo}",
                            "to tell me about {categoryTwo}",
                            "tell me about facilities offered in {categoryTwo}",
                            "{categoryTwo} facilities",
                            "facilities available in {categoryTwo}"
                        ]
                    },
                    {
                        "name": "GenericCheckStatusIntent",
                        "slots": [],
                        "samples": [
                            "what is the status of my request",
                            "what is the status of my requests",
                            "tell me the status of my requests",
                            "tell me the status of my request",
                            "tell me the status of my requests please",
                            "please tell me the status of my requests",
                            "to tell me the status of my requests please",
                            "to tell me the status of my requests",
                            "to please tell me the status of my requests",
                            "what all orders did i place",
                            "show me all my orders",
                            "show me all status",
                            "what is the status of my order",
                            "to please tell me the status of my order",
                            "to tell me the status of my order please",
                            "please tell me the status of my order",
                            "tell me the status of my order please",
                            "to tell me the status of my order",
                            "tell me the status of my order"
                        ]
                    },
                    {
                        "name": "Happy",
                        "slots": [],
                        "samples": [
                            "happy"
                        ]
                    },
                    {
                        "name": "HotelInfoIntent",
                        "slots": [],
                        "samples": [
                            "what all services this hotel provides",
                            "hotel information please",
                            "tell me about this hotel",
                            "tell me more about this hotel",
                            "tell me about hotel",
                            "tell me about the hotel",
                            "show me hotel info",
                            "show me the hotel info",
                            "hotel info",
                            "show me the hotel information",
                            "show me hotel information",
                            "hotel information"
                        ]
                    },
                    {
                        "name": "HouseKeepingService",
                        "slots": [],
                        "samples": [
                            "housekeeping",
                            "housekeeping services",
                            "housekeeping service"
                        ]
                    },
                    {
                        "name": "LaundryIntent",
                        "slots": [
                            {
                                "name": "laundry",
                                "type": "laundry"
                            },
                            {
                                "name": "clothes",
                                "type": "clothes"
                            }
                        ],
                        "samples": [
                            "yes send someone to collect my {laundry} please",
                            "yes please send someone to collect my {laundry}",
                            "yes send someone to collect my {laundry}",
                            "send someone to collect my {laundry} please",
                            "please send someone to collect my {laundry} ",
                            "to please send someone to collect my {laundry}",
                            "to send someone to collect my {laundry} please",
                            "to send someone to collect my {laundry}",
                            "yes ask someone to pick up my {laundry} please",
                            "yes please ask someone to pick up my {laundry}",
                            "yes ask someone to pick up my {laundry}",
                            "to ask someone to pick up my {laundry} please",
                            "to please ask someone to pick up my {laundry}",
                            "to ask someone to pick up my {laundry}",
                            "ask someone to pick up my {laundry} please",
                            "please ask someone to pick up my {laundry}",
                            "ask someone to pick up my {laundry} ",
                            "can i have someone to collect my {laundry} please",
                            "can i have someone to collect my {laundry} ",
                            "send housekeeping to pick up my {laundry}",
                            "to send housekeeping to pick up my {laundry}",
                            "please pick up my {clothes}",
                            "pickup my {clothes} for {laundry}",
                            "pickup my {laundry}",
                            "to please pick up my {clothes} for {laundry}",
                            "to pick up my {clothes} for {laundry} please",
                            "to pick up my {clothes} for {laundry}",
                            "please pick up my {clothes} for {laundry}",
                            "pick up my {clothes} for {laundry} please",
                            " pick up my {clothes} for {laundry} ",
                            " please pick up my {laundry}",
                            " pick up my {laundry} please",
                            " pick up my {laundry} ",
                            "to please pick up my {laundry}",
                            "to pick up my {laundry} please",
                            "to pick up my {laundry} ",
                            "to please pick up my laundry",
                            "to pick up my laundry please",
                            "to pick up my laundry",
                            "please pick up my laundry",
                            "pick up my laundry please",
                            "pick up my laundry",
                            "Can you send some one from housekeeping to collect my {laundry}",
                            "please send some one from housekeeping to collect my {laundry}",
                            "Can you please send some one from housekeeping to collect my {laundry}",
                            "please inform someone to come and collect my {laundry} from the room",
                            "please inform someone to come and collect my {laundry}",
                            "send some one to collect my {clothes} for {laundry}",
                            "please send some one to collect my {clothes} for {laundry}",
                            "send some one to collect my {clothes} for {laundry} please",
                            "send some one to take my {clothes} for {laundry}",
                            "please send some one to take my {clothes} for {laundry}",
                            "send some one to take my {clothes} for {laundry} please",
                            "send some one from housekeeping to collect my {clothes} for {laundry}",
                            "send housekeeping for {laundry}",
                            "send housekeeping to collect my {laundry}",
                            "send someone from housekeeping to collect my {laundry}",
                            "send someone from housekeeping to collect my {laundry} please",
                            "please send someone from housekeeping to collect my {laundry}",
                            "send someone from housekeeping to collect my {clothes} for {laundry}",
                            "send someone from housekeeping to collect my {clothes} for {laundry} please",
                            "please send someone from housekeeping to collect my {clothes} for {laundry}",
                            "my {laundry} please",
                            "collect my {laundry}",
                            "to send some one to collect my {clothes} for {laundry}",
                            "to send some one to collect my {clothes} for {laundry} please",
                            "to please send some one to collect my {clothes} for {laundry}",
                            "to send some one to take my {clothes} for {laundry}",
                            "to send some one to take my {clothes} for {laundry} please",
                            "to please send some one to take my {clothes} for {laundry}",
                            "to tell some one from housekeeping to collect my {clothes} for {laundry}",
                            "to tell some one from housekeeping to collect my {clothes} for {laundry} please",
                            "to please tell some one from housekeeping to collect my {clothes} for {laundry}",
                            "to send some one from housekeeping to collect my {clothes} for {laundry} please",
                            "to please send some one from housekeeping to collect my {clothes} for {laundry}",
                            "to send some one from housekeeping to collect my {clothes} for {laundry}",
                            "to please send someone from housekeeping to collect my {laundry}",
                            "to send someone from housekeeping to collect my {laundry}",
                            "to send someone from housekeeping to collect my {laundry} please",
                            "to send someone from housekeeping to collect my {clothes} for {laundry} please",
                            "to please send someone from housekeeping to collect my {clothes} for {laundry}",
                            "to please send someone from laundry to {laundry} my {clothes}",
                            "send some one from housekeeping to collect my {laundry} please",
                            "please inform someone to collect my {laundry} from the room",
                            "please inform someone to collect my {laundry} ",
                            "send someone from housekeeping to {laundry} my {clothes}",
                            "send someone to collect my {laundry}",
                            "send someone to collect my {laundry} for {clothes}",
                            "to collect my {laundry}"
                        ]
                    },
                    {
                        "name": "LaundryService",
                        "slots": [],
                        "samples": [
                            "laundry",
                            "laundry service",
                            "laundry services"
                        ]
                    },
                    {
                        "name": "nearbyIntent",
                        "slots": [],
                        "samples": [
                            "what are some nice places nearby",
                            "yes tell me about the near by places please",
                            "yes tell me about the near by places",
                            "yes please tell me about the near by places",
                            "to tell me about the near by places please",
                            "to please tell me about the near by places",
                            "to tell me about the near by places",
                            "tell me about the near by places please",
                            "please tell me about the near by places",
                            "tell me about the near by places",
                            "yes tell me about the near by locations please",
                            "yes please tell me about the near by locations",
                            "yes tell me about the near by locations",
                            "to tell me about the near by locations please",
                            "to please tell me about the near by locations",
                            "tell me about the near by locations please",
                            "please tell me about the near by locations",
                            "to tell me about the near by locations",
                            "tell me about the near by locations",
                            "yes tell me about the near by attractions please",
                            "yes please tell me about the near by attractions",
                            "yes tell me about the near by attractions",
                            "to tell me about the near by attractions please",
                            "to please tell me about the near by attractions",
                            "to tell me about the near by attractions",
                            "tell me about the near by attractions please",
                            "please tell me about the near by attractions",
                            "nearby locations",
                            "what places can i explore near by",
                            "what can i explore today",
                            "where can i go today",
                            "what are the places to travel near by",
                            "tell me about the near by attractions",
                            "what are some nice places to explore near by",
                            "what are some nice places near by",
                            "what are the near by attraction",
                            "what are the near by places",
                            "what are the nearby places",
                            "what are the nearby attraction",
                            "what are the near by attractions",
                            "to show me the nearby attractions please",
                            "to please show me the nearby attractions",
                            "to show me the nearby attractions",
                            "please show me the nearby attractions",
                            "show me the nearby attractions please",
                            "show me the nearby attractions",
                            "nearby attractions",
                            "to please tell me about the nearby attractions",
                            "to tell me about the nearby attractions please",
                            "please tell me about the nearby attractions",
                            "to tell me about the nearby attractions",
                            "tell me about the nearby attractions please",
                            "tell me about the nearby attractions",
                            "what are the nearby attractions",
                            "tell me some tourist places nearby",
                            "what are the places to visit nearby",
                            "what are the nearby tourist places"
                        ]
                    },
                    {
                        "name": "Neutral",
                        "slots": [],
                        "samples": [
                            "Neutral"
                        ]
                    },
                    {
                        "name": "NoIntent",
                        "slots": [],
                        "samples": [
                            "no",
                            "no thanks",
                            "no thank you"
                        ]
                    },
                    {
                        "name": "OrderIntent",
                        "slots": [
                            {
                                "name": "Number",
                                "type": "AMAZON.NUMBER"
                            }
                        ],
                        "samples": [
                            "order number {Number}",
                            "open order number {Number}",
                            "food order number {Number}",
                            "food order {Number}",
                            "open book spa request {Number}",
                            "book spa request {Number}",
                            "open spa request {Number}",
                            "what's in spa request {Number}"
                        ]
                    },
                    {
                        "name": "restaurantIntent",
                        "slots": [],
                        "samples": [
                            "to please show me the hotel restaurant",
                            "to show me the hotel restaurant please",
                            "to show me the hotel restaurant",
                            "please show me the hotel restaurant",
                            "show me the hotel restaurant please",
                            "show me the hotel restaurant",
                            "to please show me the hotel restaurants",
                            "to show me the hotel restaurants please",
                            "to show me the hotel restaurants",
                            "please show me the hotel restaurants",
                            "show me the hotel restaurants please",
                            "show me the hotel restaurants",
                            "what are the available restaurants in hotel",
                            "to show me the available restaurants in the hotel please",
                            "to please show me the available restaurants in the hotel",
                            "to show me the available restaurants in the hotel",
                            "please show me the available restaurants in the hotel",
                            "show me the available restaurants in the hotel please",
                            "show me the available restaurants in the hotel",
                            "to please show me the restaurants in the hotel",
                            "to show me the restaurants in the hotel please",
                            "to show me the restaurants in the hotel",
                            "please show me the restaurants in the hotel",
                            "show me the restaurants in the hotel please",
                            "show me the restaurants in the hotel",
                            "what are the available restaurants in the hotel",
                            "restaurants available",
                            "restaurants",
                            "what all restaurants available",
                            "what all restaurants available in radisson",
                            "to please show me the restaurants",
                            "to show me the restaurants",
                            "to show me the restaurants please",
                            "please show me the restaurants",
                            "show me the restaurants please",
                            "show me the restaurants",
                            "to show me the available restaurants please",
                            "to please show me the available restaurants",
                            "to show me the available restaurants",
                            "please show me the available restaurants",
                            "show me the available restaurants please",
                            "show me the available restaurants",
                            "what are the restaurants available",
                            "what are the restaurants in the hotel",
                            "what are the available restaurants"
                        ]
                    },
                    {
                        "name": "RoomInfoIntent",
                        "slots": [],
                        "samples": [
                            "to please give me some room information",
                            "to give me some room information please",
                            "to give me some room information",
                            "give me some room information",
                            "to give me some room info",
                            "give me some room info",
                            "to please tell me the types of room available",
                            "please tell me the types of room available",
                            "to tell me the types of room available",
                            "tell me the types of room available please",
                            "tell me the types of room available",
                            "to please tell me the type of rooms available",
                            "please tell me the type of rooms available",
                            "tell me the type of rooms available please",
                            "to tell me the type of rooms available",
                            "tell about the rooms",
                            "rooms type",
                            "room types",
                            "tell me the type of rooms available",
                            "room information",
                            "room info",
                            "rooms info",
                            "tell me about the available rooms",
                            "what type of rooms are available",
                            "what are the rooms available",
                            "rooms available in the hotel",
                            "can you tell me the available rooms",
                            "what are the available rooms",
                            "are there any rooms available"
                        ]
                    },
                    {
                        "name": "Sad",
                        "slots": [],
                        "samples": [
                            "Sad"
                        ]
                    },
                    {
                        "name": "SelectIntent",
                        "slots": [
                            {
                                "name": "number",
                                "type": "AMAZON.NUMBER"
                            },
                            {
                                "name": "quantity",
                                "type": "AMAZON.NUMBER",
                                "samples": [
                                    "{quantity}",
                                    "add {quantity}"
                                ]
                            }
                        ],
                        "samples": [
                            "select number {number} please please {quantity}",
                            "number {number}",
                            "select number {number} please",
                            "open {number}",
                            "open number {number}",
                            "please select number {number}",
                            "to select number {number}",
                            "to select number {number} please",
                            "to please select number {number}"
                        ]
                    },
                    {
                        "name": "ServicesIntent",
                        "slots": [
                            {
                                "name": "Complaints",
                                "type": "Complaint"
                            }
                        ],
                        "samples": [
                            "there is problem with my {Complaints}",
                            "there is a problem with my {Complaints}",
                            "yes send the {Complaints} please",
                            "yes please send the {Complaints}",
                            "yes send the {Complaints}",
                            "to send the {Complaints} please",
                            "to please send the {Complaints}",
                            "to send the {Complaints}",
                            "send the {Complaints} please",
                            "please send the {Complaints}",
                            "send the {Complaints}",
                            "yes send {Complaints} please",
                            "yes please send {Complaints}",
                            "yes send {Complaints}",
                            "send {Complaints} please",
                            "please send {Complaints}",
                            "send {Complaints}",
                            "to please send {Complaints} ",
                            "to send {Complaints} please",
                            "to send {Complaints}",
                            "yes inform that {Complaints} please",
                            "inform that {Complaints} please",
                            "please inform that {Complaints}",
                            "to inform that {Complaints}",
                            "inform that {Complaints}",
                            "yes inform that {Complaints}",
                            "yes {Complaints}",
                            "yes send a {Complaints} please",
                            "yes please send a {Complaints}",
                            "yes send a {Complaints} ",
                            "send a {Complaints} please",
                            "please send a {Complaints}",
                            "to send a {Complaints}",
                            "send a {Complaints}",
                            "to {Complaints}",
                            "there seems to be a {Complaints}",
                            "there seems to be an {Complaints}",
                            "send someone to {Complaints} please",
                            "to send someone to {Complaints}",
                            "send someone to {Complaints}",
                            "there seems to be {Complaints}",
                            "that my {Complaints}",
                            "that I am unable to {Complaints}",
                            "{Complaints}  please send a plumber",
                            "that {Complaints}  please send a plumber",
                            "that {Complaints}  Have them send a plumber",
                            "{Complaints} Have them send a plumber",
                            "that the {Complaints}",
                            "the {Complaints}",
                            "inform hotel that {Complaints}",
                            "inform someone that my {Complaints}",
                            "my {Complaints}",
                            "there is a {Complaints}",
                            "{Complaints}",
                            "that {Complaints}",
                            "could you send in someone to fix my {Complaints}",
                            "could you send in someone to fix my {Complaints} please",
                            "could you come and check my {Complaints}",
                            "could you come and check my {Complaints} please",
                            "could you inform someone that my {Complaints} properly",
                            "could you please inform someone that my {Complaints}",
                            "could you please inform someone that my {Complaints} properly",
                            "can you send in someone to fix my {Complaints}",
                            "can you send in someone to fix my {Complaints} please",
                            "can you come and check my {Complaints}",
                            "can you come and check my {Complaints} please",
                            "can you inform someone that my {Complaints} properly",
                            "can you please inform someone that my {Complaints}",
                            "can you please inform someone that my {Complaints} properly",
                            "send someone to fix my {Complaints}",
                            "send someone to fix my {Complaints} please",
                            "send someone to come and check my {Complaints}",
                            "send someone to come and check my {Complaints} please",
                            "send in someone to fix my {Complaints}",
                            "send in someone to fix my {Complaints} please",
                            "send in someone to come and check my {Complaints}",
                            "send in someone to come and check my {Complaints} please",
                            "please send someone to fix my {Complaints}",
                            "come and check my {Complaints}",
                            "come and check my {Complaints} please",
                            "that my {Complaints} is not working please",
                            "and inform that my {Complaints} properly",
                            "and please inform that my {Complaints}",
                            "and please inform that my {Complaints} properly",
                            "that {Complaints} properly",
                            "my {Complaints} is broken",
                            "that my {Complaints} is broken",
                            "inform someone that {Complaints} ",
                            "inform housekeeping that {Complaints} ",
                            "yes my {Complaints} is broken"
                        ]
                    },
                    {
                        "name": "TimingsIntent",
                        "slots": [
                            {
                                "name": "category",
                                "type": "category"
                            }
                        ],
                        "samples": [
                            "about the timings of the {category} please",
                            "about the timings of the {category}",
                            "the timings of the {category} please",
                            "the timings of the {category} ",
                            "at what time does the {category} open",
                            "at what time does the {category} opens",
                            "to please show me the {category} time",
                            "to show me the {category} time please",
                            "to show me the {category} time",
                            "please show me the {category} time",
                            "show me the {category} time please",
                            "show me the {category} time",
                            "to tell me the {category} time please",
                            "to please tell me the {category} time",
                            "please tell me the {category} time",
                            "to tell me the {category} time",
                            "tell me the {category} time please",
                            "tell me the {category} time",
                            "at what time the {category} closes",
                            "at what time the {category} opens",
                            "what are the opening and closing timing of the {category}",
                            "what are the opening and closing timings of the {category}",
                            "what are the opening and closing time of the {category}",
                            "to show me the {category} timing please",
                            "to please show me the {category} timing",
                            "please show me the {category} timing",
                            "to show me the {category} timing",
                            "show me the {category} timing please",
                            "show me the {category} timing",
                            "to please show me the {category} timings",
                            "to show me the {category} timings",
                            "to show me the {category} timings please",
                            "please show me the {category} timings",
                            "show me the {category} timings please",
                            "show me the {category} timings",
                            "{category} timing",
                            "what is the timing of the {category}",
                            "to tell me the {category} timing please",
                            "to please tell me the {category} timing",
                            "to tell me the {category} timing",
                            "please tell me the {category} timing",
                            "tell me the {category} timing please",
                            "tell me the {category} timing",
                            "to please tell me the {category} timings",
                            "please tell me the {category} timings",
                            "to tell me the {category} timings please",
                            "to tell me the {category} timings",
                            "tell me the {category} timings please",
                            "tell me the {category} timings",
                            "what are the {category} timings",
                            "{category} timings",
                            "what are the timing of {category}",
                            "what is the timing of {category}",
                            "what are the timings of {category}",
                            "timing of {category}",
                            "timings of {category}",
                            "tell me {category} timing",
                            "tell me {category} timings",
                            "tell me the timings of {category}"
                        ]
                    },
                    {
                        "name": "Valet",
                        "slots": [],
                        "samples": [
                            "valet parking"
                        ]
                    },
                    {
                        "name": "ValetParkingIntent",
                        "slots": [],
                        "samples": [
                            "valet to get my car out please",
                            "get my car out",
                            "to get my car out",
                            "valet to get my car out",
                            "i need my car to be ready at the gate",
                            "to please prepare my car ",
                            "to prepare my car please",
                            "prepare my car please",
                            "to prepare my car at the gate please",
                            "to please prepare my car at the gate",
                            "to prepare my car at the gate",
                            "please prepare my car at the gate",
                            "prepare my car at the gate please",
                            "prepare my car at the gate",
                            "prepare my car",
                            "to get my Car ready",
                            "to get my Car ready please",
                            "to please get my Car ready",
                            "get my car ready",
                            "please get my car ready",
                            "keep my car ready",
                            "keep my car ready please",
                            "please keep my car ready",
                            "to keep my car ready",
                            "to please keep my car ready",
                            "to keep my car ready please",
                            "to get my car ready at the gate",
                            "to get my car ready at the gate please",
                            "to please get my car ready at the gate ",
                            "please get my car ready at the gate",
                            "get my car ready at the gate",
                            "get my car ready at the gate please",
                            "keep my car ready at the gate",
                            "keep my car ready at the gate please",
                            "to keep my car ready at the gate",
                            "to keep my car ready at the gate please",
                            "to please keep my car ready at the gate",
                            "tell the valet to get my car ready",
                            "send a valet to get my car"
                        ]
                    },
                    {
                        "name": "YesIntent",
                        "slots": [],
                        "samples": [
                            "yep",
                            "yes i want to give feedback",
                            "okay yes",
                            "yes",
                            "yes please",
                            "yeah",
                            "ok",
                            "sure"
                        ]
                    },
                    {
                        "name": "ConciergeIntent",
                        "slots": [
                            {
                                "name": "Destination",
                                "type": "destination"
                            },
                            {
                                "name": "time",
                                "type": "AMAZON.TIME",
                                "samples": [
                                    "at {time} please",
                                    "{time} please",
                                    "at {time}",
                                    "{time}"
                                ]
                            },
                            {
                                "name": "date",
                                "type": "AMAZON.DATE",
                                "samples": [
                                    "on {date} please",
                                    "{date} please",
                                    "on {date}",
                                    "{date}"
                                ]
                            }
                        ],
                        "samples": [
                            "yes book a cab to {Destination} at {time} {date}  please",
                            "yes please book a cab to {Destination} at {time} {date} ",
                            "yes book a cab to {Destination} at {time} {date} ",
                            "to book a cab to {Destination} at {time} {date}  please",
                            "to please book a cab to {Destination} at {time} {date} ",
                            "book a cab to {Destination} at {time} {date}  please",
                            "please book a cab to {Destination} at {time} {date} ",
                            "to book a cab to {Destination} at {time} {date} ",
                            "book a cab to {Destination} at {time} {date} ",
                            "yes book a cab at {time} {date} please",
                            "yes please book a cab at {time} {date}",
                            "yes book a cab at {time} {date}",
                            "to book a cab at {time} {date} please",
                            "to please book a cab at {time} {date}",
                            "to book a cab at {time} {date}",
                            "book a cab at {time} {date} please",
                            "please book a cab at {time} {date}",
                            "book a cab at {time} {date} ",
                            "yes book a cab at {time} please",
                            "yes please book a cab at {time}",
                            "yes book a cab at {time}",
                            "to book a cab at {time} please",
                            "to please book a cab at {time}",
                            "book a cab at {time} please",
                            "please book a cab at {time}",
                            "to book a cab at {time}",
                            "book a cab at {time}",
                            "yes schedule a cab for me to {Destination} at {time} {date} please",
                            "yes please schedule a cab for me to {Destination} at {time} {date}",
                            "yes schedule a cab for me to {Destination} at {time} {date}",
                            "to schedule a cab for me to {Destination} at {time} {date} please",
                            "to please schedule a cab for me to {Destination} at {time} {date}",
                            "schedule a cab for me to {Destination} at {time} {date} please",
                            "please schedule a cab for me to {Destination} at {time} {date}",
                            "to schedule a cab for me to {Destination} at {time} {date}",
                            "schedule a cab for me to {Destination} at {time} {date}",
                            "yes schedule a cab for me to {Destination} at {time} please",
                            "yes please schedule a cab for me to {Destination} at {time}",
                            "yes schedule a cab for me to {Destination} at {time}",
                            "to schedule a cab for me to {Destination} at {time} please",
                            "to please schedule a cab for me to {Destination} at {time}",
                            "to schedule a cab for me to {Destination} at {time}",
                            "schedule a cab for me to {Destination} at {time} please",
                            "please schedule a cab for me to {Destination} at {time}",
                            "schedule a cab for me to {Destination} at {time} ",
                            "yes schedule a cab for me to {Destination} please",
                            "yes please schedule a cab for me to {Destination}",
                            "yes schedule a cab for me to {Destination}",
                            "to schedule a cab for me to {Destination} please",
                            "to please schedule a cab for me to {Destination}",
                            "schedule a cab for me to {Destination} please",
                            "please schedule a cab for me to {Destination}",
                            "to schedule a cab for me to {Destination}",
                            "schedule a cab for me to {Destination} ",
                            "yes I need a cab to {Destination} at {time} {date}",
                            "yes I need a cab to {Destination} at {time}",
                            "I need a cab to {Destination} at {time} {date}",
                            "I need a cab to {Destination} at {time} ",
                            "yes book a cab for me for {time} {date} to {Destination} please",
                            "yes please book a cab for me for {time} {date} to {Destination}",
                            "yes book a cab for me for {time} {date} to {Destination}",
                            "to book a cab for me for {time} {date} to {Destination} please",
                            "to please book a cab for me for {time} {date} to {Destination}",
                            "book a cab for me for {time} {date} to {Destination} please",
                            "please book a cab for me for {time} {date} to {Destination}",
                            "to book a cab for me for {time} {date} to {Destination}",
                            "book a cab for me for {time} {date} to {Destination} ",
                            "yes book a cab for me for {time} {date} please",
                            "yes please book a cab for me for {time} {date}",
                            "yes book a cab for me for {time} {date}",
                            "to please book a cab for me for {time} {date}",
                            "to book a cab for me for {time} {date} please",
                            "book a cab for me for {time} {date} please",
                            "please book a cab for me for {time} {date}",
                            "to book a cab for me for {time} {date}",
                            "book a cab for me for {time} {date} ",
                            "to book a cab for me at {time} to {Destination} please",
                            "to please book a cab for me at {time} to {Destination}",
                            "to book a cab for me at {time} to {Destination}",
                            "book a cab for me at {time} to {Destination} please",
                            "please book a cab for me at {time} to {Destination}",
                            "book a cab for me at {time} to {Destination} ",
                            "yes arrange a cab to {Destination} at {time} please",
                            "yes please arrange a cab to {Destination} at {time}",
                            "yes arrange a cab to {Destination} at {time}",
                            "to arrange a cab to {Destination} at {time} please",
                            "to please arrange a cab to {Destination} at {time}",
                            "arrange a cab to {Destination} at {time} please",
                            "please arrange a cab to {Destination} at {time}",
                            "to arrange a cab to {Destination} at {time}",
                            "arrange a cab to {Destination} at {time} ",
                            "yes arrange a cab to {Destination} for {time}  please",
                            "yes please arrange a cab to {Destination} for {time} ",
                            "yes arrange a cab to {Destination} for {time} ",
                            "arrange a cab to {Destination} for {time}  please",
                            "please arrange a cab to {Destination} for {time} ",
                            "to arrange a cab to {Destination} for {time}  please",
                            "to please arrange a cab to {Destination} for {time} ",
                            "to arrange a cab to {Destination} for {time} ",
                            "arrange a cab to {Destination} for {time} ",
                            "yes arrange a cab to {Destination} for {time} {date} please",
                            "yes please arrange a cab to {Destination} for {time} {date}",
                            "yes arrange a cab to {Destination} for {time} {date}",
                            "to arrange a cab to {Destination} for {time} {date} please",
                            "to please arrange a cab to {Destination} for {time} {date}",
                            "arrange a cab to {Destination} for {time} {date} please",
                            "please arrange a cab to {Destination} for {time} {date}",
                            "to arrange a cab to {Destination} for {time} {date}",
                            "arrange a cab to {Destination} for {time} {date} ",
                            "yes arrange a cab to {Destination} please",
                            "yes please arrange a cab to {Destination}",
                            "yes arrange a cab to {Destination}",
                            "to arrange a cab to {Destination} please",
                            "to please arrange a cab to {Destination}",
                            "to arrange a cab to {Destination}",
                            "yes book me a cab to {Destination} at {time} ",
                            "yes please book me a cab to {Destination} at {time} ",
                            "yes book me a cab to {Destination} at {time} please",
                            "yes i want to book a cab to {Destination} at {time} {date}",
                            "yes i want to book a cab to {Destination} at {time}",
                            "yes please book a cab to {Destination} for {time} {date}",
                            "yes book a cab to {Destination} for {time} {date} please",
                            "yes book a cab to {Destination} for {time} {date}",
                            "to book a cab to {Destination} for {time} {date} please",
                            "to please book a cab to {Destination} for {time} {date}",
                            "book a cab to {Destination} for {time} {date} please",
                            "please book a cab to {Destination} for {time} {date}",
                            "to book a cab to {Destination} for {time} {date}",
                            "book a cab to {Destination} for {time} {date} ",
                            "i want to book a cab to {Destination} at {time} on {date}",
                            "i want to book a cab to {Destination} at {time}",
                            "to book me a cab to {Destination} at {time} please",
                            "to please book me a cab to {Destination} at {time}",
                            "book me a cab to {Destination} at {time} please",
                            "please book me a cab to {Destination} at {time}",
                            "to book me a cab to {Destination} at {time}",
                            "book me a cab to {Destination} at {time} ",
                            "to book a cab to the {Destination}",
                            "to book a cab to the {Destination} please",
                            "to please book a cab to the {Destination}",
                            "book a cab to the {Destination} please",
                            "please book a cab to the {Destination}",
                            "to book a cab for me to the {Destination} please",
                            "to please book a cab for me to the {Destination}",
                            "yes book a cab for me to the {Destination}",
                            "book a cab for me to the {Destination} please",
                            "please book a cab for me to the {Destination}",
                            "to book a cab for me to the {Destination}",
                            "book a cab to the {Destination}",
                            "book a cab for me to the {Destination}",
                            "i would like to book a cab for {Destination} at {time}",
                            "i would like to book a cab for {Destination}",
                            "i want to book a cab",
                            "book a cab to {Destination} at {time} please",
                            "book a cab to {Destination} at {time}",
                            "book a cab to {Destination}",
                            "book a cab for me to the {Destination} at {time}",
                            "please arrange a cab to {Destination}",
                            "I need a ride for {date} at {time} to {Destination}",
                            "i need a ride to {Destination}",
                            "could you please get me a cab to {Destination}",
                            "could you please arrange my cab for {date} at {time}",
                            "please arrange my cab at {time} evening",
                            "i need a cab to {Destination}",
                            "I would like to schedule a cab to {Destination} for {time} {date}",
                            "I would like to schedule a cab to {Destination} at {time} {date}",
                            "I would like to schedule a cab for {time} {date}",
                            "I would like to schedule a cab for {date} ",
                            "I would like to schedule a cab for {date} {time}",
                            "I would like to book a cab for {date} {time}",
                            "I would like to book a cab for {date} at {time}",
                            "I would like to book a cab at {time} {date}",
                            "I would like to book a cab",
                            "I want to book a cab at {time} {date}",
                            "I want to book a cab at {time} ",
                            "I want to book a cab for {date} {time}",
                            "i wannt to book a cab",
                            "to please book a cab for {date} {time}",
                            "to book a cab for {date} {time} please",
                            "to book a cab for {date} {time}",
                            "please book a cab for {date} {time}",
                            "book a cab for {date} {time} please",
                            "book a cab for {date} {time} ",
                            "to book a cab please",
                            "to please book a cab",
                            "to book a cab",
                            "please book a cab",
                            "book a cab please",
                            "book a cab",
                            "to please book a cab for me at {time} {date}",
                            "to book a cab for me at {time} {date} please",
                            "to book a cab for me at {time} {date}",
                            "please book a cab for me at {time} {date}",
                            "book a cab for me at {time} {date} please",
                            "book a cab for me at {time} {date}",
                            "to book a cab for me for {date} {time} please",
                            "to please book a cab for me for {date} {time}",
                            "to book a cab for me for {date} {time}",
                            "please book a cab for me for {date} {time}",
                            "book a cab for me for {date} {time} please",
                            "book a cab for me for {date} {time} ",
                            "to please book a cab for me",
                            "to book a cab for me please",
                            "to book a cab for me",
                            "please book a cab for me",
                            "book a cab for me please",
                            "book a cab for me",
                            "to book me a cab for {Destination} at {time} {date}",
                            "to book me a cab for {Destination} at {time} {date} please",
                            "to please book me a cab for {Destination} at {time} {date}",
                            "please book me a cab for {Destination} at {time} {date}",
                            "book me a cab for {Destination} at {time} {date} please",
                            "book me a cab for {Destination} at {time} {date} ",
                            "to book me a cab for {Destination} at {time} please",
                            "to please book me a cab for {Destination} at {time}",
                            "to book me a cab for {Destination} at {time}",
                            "please book me a cab for {Destination} at {time}",
                            "book me a cab for {Destination} at {time} please",
                            "book me a cab for {Destination} at {time} ",
                            "to book me a cab to {Destination} for {date} {time} please",
                            "to please book me a cab to {Destination} for {date} {time}",
                            "to book me a cab to {Destination} for {date} {time}",
                            "please book me a cab to {Destination} for {date} {time}",
                            "book me a cab to {Destination} for {date} {time} please",
                            "book me a cab to {Destination} for {date} {time} ",
                            "to please book me a cab to {Destination} for {time} {date}",
                            "to book me a cab to {Destination} for {time} {date} please",
                            "to book me a cab to {Destination} for {time} {date}",
                            "please book me a cab to {Destination} for {time} {date}",
                            "book me a cab to {Destination} for {time} {date} please",
                            "book me a cab to {Destination} for {time} {date} ",
                            "to book me a cab to {Destination} please",
                            "to please book me a cab to {Destination}",
                            "to book me a cab to {Destination}",
                            "please book me a cab to {Destination}",
                            "book me a cab to {Destination} please",
                            "book me a cab to {Destination}"
                        ]
                    },
                    {
                        "name": "OrderFood",
                        "slots": [],
                        "samples": [
                            "i want to order food"
                        ]
                    },
                    {
                        "name": "WakeUpCallIntent",
                        "slots": [
                            {
                                "name": "Time",
                                "type": "AMAZON.TIME",
                                "samples": [
                                    "{Time} at night",
                                    "{Time} in the evening",
                                    "{Time} in the morning",
                                    "{Time}",
                                    "at {Time}"
                                ]
                            },
                            {
                                "name": "Date",
                                "type": "AMAZON.DATE",
                                "samples": [
                                    "{Date} please",
                                    "{Date} night",
                                    "{Date} afternoon",
                                    "{Date} evening",
                                    "{Date} morning",
                                    "on {Date}",
                                    "{Date}"
                                ]
                            },
                            {
                                "name": "SecondWakeCall",
                                "type": "YesOrNo",
                                "samples": [
                                    "please {SecondWakeCall}",
                                    "{SecondWakeCall} please",
                                    "{SecondWakeCall} thank you"
                                ]
                            },
                            {
                                "name": "TeaCoffee",
                                "type": "TeaCoffee",
                                "samples": [
                                    "yes i want to have {TeaCoffee}",
                                    "yes i want {TeaCoffee}",
                                    "yes {TeaCoffee} please",
                                    "{TeaCoffee} thank you",
                                    "{TeaCoffee} please",
                                    "{TeaCoffee}",
                                    "yes {TeaCoffee}"
                                ]
                            }
                        ],
                        "samples": [
                            "to wake me up in morning",
                            "please set a wake me up to call the as we have done on {Date} at {Time} please please please please please please please please please please please please please please {SecondWakeCall} {TeaCoffee}",
                            "place a wake up call",
                            "wake me up",
                            "to set a wake up call on {Date} at {Time} please",
                            "to please set a wake up call on {Date} at {Time} ",
                            "to set a wake up call on {Date} at {Time}",
                            "please set a wake up call on {Date} at {Time} ",
                            "set a wake up call on {Date} at {Time} please",
                            "set a wake up call on {Date} at {Time} ",
                            "to set a wake up call at {Time} please",
                            "to please set a wake up call at {Time} ",
                            "please set a wake up call at {Time} ",
                            "set a wake up call at {Time} please",
                            "to set a wake up call at {Time} ",
                            "set a wake up call at {Time} ",
                            "to wake me up {Date} at {Time} please",
                            "to please wake me up {Date} at {Time}",
                            "please wake me up {Date} at {Time}",
                            "wake me up {Date} at {Time} please",
                            "to wake me up {Date} at {Time} ",
                            "to please set up a wake up call for {Date} at {Time}",
                            "to set up a wake up call for {Date} at {Time} please",
                            "please set up a wake up call for {Date} at {Time}",
                            "set up a wake up call for {Date} at {Time} please",
                            "to set up a wake up call for {Date} at {Time}",
                            "set up a wake up call for {Date} at {Time} ",
                            "to set a wake up call at {Time} {Date} please",
                            "to please set a wake up call at {Time} {Date}",
                            "please set a wake up call at {Time} {Date}",
                            "set a wake up call at {Time} {Date} please",
                            "to set a wake up call at {Time} {Date}",
                            "set a wake up call at {Time} {Date} ",
                            "to wake me up at {Time} {Date} please",
                            "to please wake me up at {Time} {Date} ",
                            "please wake me up at {Time} {Date}",
                            "wake me up at {Time} {Date} please",
                            "to wake me up at {Time} {Date} ",
                            "wake me up at {Time} {Date}",
                            "wakeup call request",
                            "i want to place a wakeup call",
                            "place a wakeup call request",
                            "give me a wakeup call at {Time}",
                            "place a wake up call request",
                            "i want to place a wake up call",
                            "wake me up in morning",
                            "to wake me up please",
                            "to wake me up at {Time} please",
                            "to wake me up",
                            "can you wake me up",
                            "please wake me up",
                            "wake up call request",
                            "i want to place a wake up call request",
                            "to wake me up at {Time}",
                            "please wake me up at {Time}",
                            "give me a wake up call",
                            "give me a wake up call at {Time}",
                            "wake me up at {Time}"
                        ]
                    },
                    {
                        "name": "WifiIntent",
                        "slots": [],
                        "samples": [
                            "how to connect to wi fi",
                            "what is the wi fi password",
                            "how do i connect to wi fi",
                            "help me connect to wi fi",
                            "how can i connect to wi fi",
                            "help me connect to wifi",
                            "how can i connect to wifi",
                            "how do i connect to internet",
                            "how do i connect to wifi",
                            "how to connect to the internet",
                            "how to connect to wifi",
                            "what is the wifi password"
                        ]
                    },
                    {
                        "name": "HousekeepingIntent",
                        "slots": [
                            {
                                "name": "Housekeeping",
                                "type": "Housekeeping"
                            }
                        ],
                        "samples": [
                            "yes send room service to {Housekeeping} please",
                            "yes please send room service to {Housekeeping}",
                            "yes send room service to {Housekeeping}",
                            "to send room service to {Housekeeping} please",
                            "to please send room service to {Housekeeping}",
                            "to send room service to {Housekeeping}",
                            "send room service to {Housekeeping} please",
                            "please send room service to {Housekeeping}",
                            "send room service to {Housekeeping} ",
                            "yes send room service {Housekeeping} please",
                            "yes please send room service {Housekeeping}",
                            "yes send room service {Housekeeping}",
                            "to send room service {Housekeeping} please",
                            "to please send room service {Housekeeping}",
                            "send room service {Housekeeping} please",
                            "please send room service {Housekeeping}",
                            "to send room service {Housekeeping}",
                            "send room service {Housekeeping}",
                            "yes send housekeeping to {Housekeeping} please",
                            "yes please send housekeeping to {Housekeeping}",
                            "yes send housekeeping to {Housekeeping}",
                            "to send housekeeping to {Housekeeping} please",
                            "to please send housekeeping to {Housekeeping}",
                            "send housekeeping to {Housekeeping} please",
                            "please send housekeeping to {Housekeeping}",
                            "to send housekeeping to {Housekeeping}",
                            "send housekeeping to {Housekeeping} ",
                            "yes {Housekeeping}",
                            "to {Housekeeping} please",
                            "to send someone for {Housekeeping} please",
                            "to please send someone for {Housekeeping}",
                            "to send someone for {Housekeeping}",
                            "yes send {Housekeeping} please",
                            "yes please send {Housekeeping}",
                            "yes send {Housekeeping}",
                            "please send {Housekeeping}",
                            "to send {Housekeeping} please",
                            "to please send {Housekeeping} ",
                            "to send someone from {Housekeeping}",
                            "to send someone {Housekeeping}",
                            "send a {Housekeeping}",
                            "send someone from {Housekeeping}",
                            "send {Housekeeping} please",
                            "send {Housekeeping}",
                            "to send {Housekeeping}",
                            "can you please send someone to {Housekeeping} please",
                            "i want {Housekeeping} as well",
                            "i want {Housekeeping} also",
                            "can i have {Housekeeping}",
                            "i need {Housekeeping}",
                            "i want {Housekeeping}",
                            "can you please send someone to {Housekeeping} as well",
                            "can you please send someone to {Housekeeping}",
                            "{Housekeeping}",
                            "send someone to {Housekeeping} my room",
                            "send someone from housekeeping to {Housekeeping} my room",
                            "to {Housekeeping}",
                            "{Housekeeping} my room please",
                            "{Housekeeping} my room",
                            "send some one for {Housekeeping}",
                            "{Housekeeping} password",
                            "{Housekeeping} credentials",
                            "to change my {Housekeeping}",
                            "to change my {Housekeeping} please",
                            "to clean my {Housekeeping}",
                            "to clean my {Housekeeping} please",
                            "could you send in some extra {Housekeeping}",
                            "could you send in some extra {Housekeeping} please",
                            "could you send in some {Housekeeping} ",
                            "could you send in some {Housekeeping} please",
                            "could you order me a {Housekeeping} please",
                            "could you order me a {Housekeeping} ",
                            "could you bring me some {Housekeeping} cubes",
                            "could you bring me some {Housekeeping} cubes please",
                            "could you bring me some {Housekeeping} packets",
                            "could you bring me some {Housekeeping} packets please",
                            "could you bring me a {Housekeeping} please",
                            "could you bring me a {Housekeeping}",
                            "could you send in someone to change my {Housekeeping}",
                            "could you send in someone to change my {Housekeeping} please",
                            "could you get me some {Housekeeping}",
                            "could you get me some {Housekeeping} please",
                            "could you come and check my {Housekeeping}",
                            "could you come and check my {Housekeeping} please",
                            "could you come help me with {Housekeeping}",
                            "could you come help me arrange my {Housekeeping}",
                            "could you send in someone to clean my {Housekeeping}",
                            "could you send in someone to clean my {Housekeeping} please",
                            "could you send some one for {Housekeeping}",
                            "could you send someone to {Housekeeping} my room",
                            "could you send someone for {Housekeeping} my room",
                            "could you tell me my {Housekeeping} password",
                            "could you tell me my {Housekeeping} credentials",
                            "could you tell me {Housekeeping} password please",
                            "could you tell me {Housekeeping} credentials please",
                            "could you send in someone to change {Housekeeping}",
                            "could you send in someone to change {Housekeeping} please",
                            "could you get me {Housekeeping}",
                            "could you get me {Housekeeping} please",
                            "could you send some one for {Housekeeping} please",
                            "could you send someone to {Housekeeping} my room please",
                            "could you send someone for {Housekeeping} my room please",
                            "could you tell me my {Housekeeping} pin",
                            "could you tell me {Housekeeping} pin please",
                            "can you send in some extra {Housekeeping}",
                            "can you send in some extra {Housekeeping} please",
                            "can you send in some {Housekeeping} ",
                            "can you send in some {Housekeeping} please",
                            "can you order me a {Housekeeping} please",
                            "can you order me a {Housekeeping} ",
                            "can you send in someone to fix my {Housekeeping}",
                            "can you send in someone to fix my {Housekeeping} please",
                            "can you bring me some {Housekeeping} cubes",
                            "can you bring me some {Housekeeping} cubes please",
                            "can you bring me some {Housekeeping} packets",
                            "can you bring me some {Housekeeping} packets please",
                            "can you bring me a {Housekeeping} please",
                            "can you bring me a {Housekeeping}",
                            "can you send in someone to change my {Housekeeping}",
                            "can you send in someone to change my {Housekeeping} please",
                            "can you get me some {Housekeeping}",
                            "can you get me some {Housekeeping} please",
                            "can you come and check my {Housekeeping}",
                            "can you come and check my {Housekeeping} please",
                            "can you come help me with {Housekeeping}",
                            "can you come help me arrange my {Housekeeping}",
                            "can you send in someone to clean my {Housekeeping}",
                            "can you send in someone to clean my {Housekeeping} please",
                            "can you send some one for {Housekeeping}",
                            "can you send someone to {Housekeeping} my room",
                            "can you send someone for {Housekeeping} my room",
                            "can you tell me my {Housekeeping} password",
                            "can you tell me my {Housekeeping} credentials",
                            "can you tell me {Housekeeping} password please",
                            "can you tell me {Housekeeping} credentials please",
                            "can you send in someone to change {Housekeeping}",
                            "can you send in someone to change {Housekeeping} please",
                            "can you get me {Housekeeping}",
                            "can you get me {Housekeeping} please",
                            "can you send some one for {Housekeeping} please",
                            "can you send someone to {Housekeeping} my room please",
                            "can you send someone for {Housekeeping} my room please",
                            "can you tell me my {Housekeeping} pin",
                            "can you tell me {Housekeeping} pin please",
                            "send some extra {Housekeeping}",
                            "send some extra {Housekeeping} please",
                            "send some {Housekeeping} ",
                            "send some {Housekeeping} please",
                            "send me a {Housekeeping} please",
                            "send me a {Housekeeping}",
                            "send someone to fix my {Housekeeping}",
                            "send someone to fix my {Housekeeping} please",
                            "send me some {Housekeeping} cubes",
                            "send me some {Housekeeping} cubes please",
                            "send me some {Housekeeping} packets",
                            "send me some {Housekeeping} packets please",
                            "send someone to change my {Housekeeping}",
                            "send someone to change my {Housekeeping} please",
                            "send me some {Housekeeping}",
                            "send me some {Housekeeping} please",
                            "send someone to come and check my {Housekeeping}",
                            "send someone to come and check my {Housekeeping} please",
                            "send someone to come help me with {Housekeeping}",
                            "send someone to clean my {Housekeeping}",
                            "send someone to clean my {Housekeeping} please",
                            "send someone for {Housekeeping}",
                            "send me my {Housekeeping} password",
                            "send me my {Housekeeping} credentials",
                            "send me {Housekeeping} password please",
                            "send me {Housekeeping} credentials please",
                            "send someone to change {Housekeeping}",
                            "send someone to change {Housekeeping} please",
                            "send me {Housekeeping}",
                            "send me {Housekeeping} please",
                            "send someone to bring me {Housekeeping} cubes",
                            "send someone to bring me {Housekeeping} cubes please",
                            "send someone to bring me {Housekeeping} packets",
                            "send someone to bring me {Housekeeping} packets please",
                            "send someone to get me {Housekeeping} cubes",
                            "send someone to get me {Housekeeping} cubes please",
                            "send someone to get me {Housekeeping} packets",
                            "send someone to get me {Housekeeping} packets please",
                            "send someone to bring me {Housekeeping} please",
                            "send someone to bring me {Housekeeping}",
                            "send someone to get me {Housekeeping}",
                            "send someone to get me {Housekeeping} please",
                            "send in someone to change my {Housekeeping}",
                            "send in someone to change my {Housekeeping} please",
                            "send in someone to come and check my {Housekeeping}",
                            "send in someone to come and check my {Housekeeping} please",
                            "send in someone to come help me with {Housekeeping}",
                            "send in someone to come help me arrange my {Housekeeping}",
                            "send in someone to clean my {Housekeeping}",
                            "send in someone to clean my {Housekeeping} please",
                            "send in someone for {Housekeeping}",
                            "send in someone to {Housekeeping} my room",
                            "send in someone to change {Housekeeping}",
                            "send in someone to change {Housekeeping} please",
                            "send in someone to bring me {Housekeeping} cubes",
                            "send in someone to bring me {Housekeeping} cubes please",
                            "send in someone to bring me {Housekeeping} packets",
                            "send in someone to bring me {Housekeeping} packets please",
                            "send in someone to get me {Housekeeping} cubes",
                            "send in someone to get me {Housekeeping} cubes please",
                            "send in someone to get me {Housekeeping} packets",
                            "send in someone to get me {Housekeeping} packets please",
                            "send in someone to bring me {Housekeeping} please",
                            "send in someone to bring me {Housekeeping}",
                            "send in someone to get me {Housekeeping}",
                            "send in someone to get me {Housekeeping} please",
                            "send in a {Housekeeping} please",
                            "send in a {Housekeeping}",
                            "send in some {Housekeeping} cubes",
                            "send in some {Housekeeping} cubes please",
                            "send in some {Housekeeping} packets",
                            "send in some {Housekeeping} packets please",
                            "send in some {Housekeeping}",
                            "send in some {Housekeeping} please",
                            "send in my {Housekeeping} password",
                            "send in my {Housekeeping} credentials",
                            "send in {Housekeeping} password please",
                            "send in {Housekeeping} credentials please",
                            "send in {Housekeeping}",
                            "send in {Housekeeping} please",
                            "please send some extra {Housekeeping}",
                            "please send some {Housekeeping}",
                            "please send me a {Housekeeping}",
                            "please send me some {Housekeeping} cubes",
                            "please send me some {Housekeeping} packets",
                            "please send someone to bring me {Housekeeping} cubes",
                            "please send someone to bring me {Housekeeping} packets",
                            "please send someone to change my {Housekeeping}",
                            "please send me some {Housekeeping}",
                            "please send someone to get me {Housekeeping} cubes",
                            "please send someone to get me {Housekeeping} packets",
                            "please send someone to bring me {Housekeeping}",
                            "please send someone to get me {Housekeeping}",
                            "please send someone to clean my {Housekeeping}",
                            "please send someone to clean my {Housekeeping} please",
                            "please send someone for {Housekeeping}",
                            "please send someone to {Housekeeping} my room",
                            "please send someone for {Housekeeping} my room",
                            "please send me my {Housekeeping} password",
                            "please send me my {Housekeeping} credentials",
                            "please send me {Housekeeping} password please",
                            "please send me {Housekeeping} credentials please",
                            "please send someone to change {Housekeeping}",
                            "please send someone to change {Housekeeping} please",
                            "please send me {Housekeeping}",
                            "please send me {Housekeeping} please",
                            "ask for my {Housekeeping} password",
                            "ask for my {Housekeeping} password please",
                            "ask for my {Housekeeping} pin",
                            "ask for my {Housekeeping} pin please",
                            "ask for my {Housekeeping} credentials please",
                            "ask for my {Housekeeping} credentials",
                            "to change {Housekeeping} ",
                            "to change {Housekeeping} please",
                            "ask for {Housekeeping} password",
                            "get me some {Housekeeping}",
                            "get me some {Housekeeping} please",
                            "I can't find my {Housekeeping}",
                            "my {Housekeeping} is broken",
                            "inform someone to {Housekeeping} my room",
                            "inform housekeeping to {Housekeeping} my room",
                            "to {Housekeeping} my room",
                            "to send someone to {Housekeeping} my room"
                        ]
                    },
                    {
                        "name": "NearbyCabIntent",
                        "slots": [
                            {
                                "name": "Time",
                                "type": "AMAZON.TIME",
                                "samples": [
                                    "{Time} in morning",
                                    "{Time} in evening",
                                    "{Time}",
                                    "at {Time}"
                                ]
                            },
                            {
                                "name": "Date",
                                "type": "AMAZON.DATE",
                                "samples": [
                                    "{Date} please",
                                    "{Date}"
                                ]
                            }
                        ],
                        "samples": [
                            "please at {Time} {Date}"
                        ]
                    },
                    {
                        "name": "OrderFoodIntent",
                        "slots": [
                            {
                                "name": "Quantity",
                                "type": "AMAZON.NUMBER"
                            },
                            {
                                "name": "Food",
                                "type": "Food"
                            }
                        ],
                        "samples": [
                            "{Quantity} more {Food}",
                            "add {Quantity} more {Food} to my food cart",
                            "add {Quantity} more {Food} to my cart",
                            "add {Quantity} more {Food} to the cart",
                            "add {Quantity} more {Food} to the food cart",
                            "add {Quantity} more {Food}",
                            "i would like to have a glass of {Food}",
                            "{Quantity} {Food} please",
                            "i want {Food}",
                            "i want {Quantity} {Food}",
                            "i'd love to have {Quantity} {Food}",
                            "get me {Food}",
                            "i need {Quantity} {Food} please",
                            "i need {Food}",
                            "to get me  {Quantity} {Food}",
                            "to place an order for {Quantity} {Food} please",
                            "to please place an order for {Quantity} {Food}",
                            "please place an order for {Quantity} {Food}",
                            "place an order for {Quantity} {Food} please",
                            "place an order for {Quantity} {Food} ",
                            "please place an order for a {Food}",
                            "place an order for a {Food} please",
                            "place an order for a {Food}",
                            "to please place an order for a {Food}",
                            "to place an order for a {Food} please",
                            "to place an order for {Quantity} {Food}",
                            "to place an order for a {Food}",
                            "yes add {Quantity} {Food} please",
                            "yes add {Quantity} {Food}",
                            "yes add {Food}",
                            "order {Quantity} {Food} please",
                            "order {Quantity} {Food}",
                            "yes order {Quantity} {Food} ",
                            "i would like to have {Quantity} {Food} please",
                            "i want to order {Quantity} {Food}",
                            "i would like to order {Quantity} {Food}",
                            "i wanna order {Quantity} {Food}",
                            "i would like to have {Quantity} {Food}",
                            "i would like to have {Food}",
                            "and {Quantity} {Food}",
                            "{Quantity} {Food}",
                            "add {Quantity} {Food}",
                            "i want to have {Food}",
                            "i would like to have a {Food}",
                            "order {Food}",
                            "to order {Food}",
                            "please add {Quantity} {Food}",
                            "please add {Food}",
                            "i would like to order {Food}",
                            "i want to order {Food}",
                            "i want to have {Quantity} {Food}",
                            "to add {Food}",
                            "to add {Quantity} {Food}",
                            "i need {Quantity} {Food}"
                        ]
                    }
                ],
                "types": [
                    {
                        "values": [
                            {
                                "name": {
                                    "value": "bookTable",
                                    "synonyms": [
                                        "book table",
                                        "table",
                                        "table booking"
                                    ]
                                }
                            },
                            {
                                "name": {
                                    "value": "bookSpa",
                                    "synonyms": [
                                        "book spa",
                                        "spa",
                                        "spa booking"
                                    ]
                                }
                            }
                        ],
                        "name": "Book"
                    },
                    {
                        "values": [
                            {
                                "name": {
                                    "value": "spa"
                                }
                            },
                            {
                                "name": {
                                    "value": "gym"
                                }
                            },
                            {
                                "name": {
                                    "value": "restaurant"
                                }
                            }
                        ],
                        "name": "category"
                    },
                    {
                        "values": [
                            {
                                "name": {
                                    "value": "times now"
                                }
                            },
                            {
                                "name": {
                                    "value": "sun tv"
                                }
                            },
                            {
                                "name": {
                                    "value": "start plus"
                                }
                            },
                            {
                                "name": {
                                    "value": "colors"
                                }
                            },
                            {
                                "name": {
                                    "value": "gemini tv"
                                }
                            },
                            {
                                "name": {
                                    "value": "zee tv"
                                }
                            },
                            {
                                "name": {
                                    "value": "sony pal"
                                }
                            },
                            {
                                "name": {
                                    "value": "star maa"
                                }
                            },
                            {
                                "name": {
                                    "value": "sony"
                                }
                            },
                            {
                                "name": {
                                    "value": "zee telugu"
                                }
                            }
                        ],
                        "name": "channelList"
                    },
                    {
                        "values": [
                            {
                                "id": "SHIRT",
                                "name": {
                                    "value": "shirt",
                                    "synonyms": [
                                        "T shirt"
                                    ]
                                }
                            },
                            {
                                "id": "TROUSER",
                                "name": {
                                    "value": "trouser",
                                    "synonyms": [
                                        "pant",
                                        "jeans"
                                    ]
                                }
                            },
                            {
                                "id": "SAREE",
                                "name": {
                                    "value": "saree"
                                }
                            },
                            {
                                "id": "FORMALSUIT",
                                "name": {
                                    "value": "FormalSuit",
                                    "synonyms": [
                                        "3 piece suit",
                                        "suit",
                                        "2 piece suit"
                                    ]
                                }
                            },
                            {
                                "id": "KURTI",
                                "name": {
                                    "value": "kurti",
                                    "synonyms": [
                                        "kurta"
                                    ]
                                }
                            },
                            {
                                "id": "BOTTOMWEAR",
                                "name": {
                                    "value": "BottomWear",
                                    "synonyms": [
                                        "herum",
                                        "skirt",
                                        "formal skirt",
                                        "pencil skirt",
                                        "palazzo",
                                        "leggings",
                                        "jeggings"
                                    ]
                                }
                            },
                            {
                                "name": {
                                    "value": "jumpsuit"
                                }
                            }
                        ],
                        "name": "clothes"
                    },
                    {
                        "values": [
                            {
                                "name": {
                                    "value": "now"
                                }
                            },
                            {
                                "name": {
                                    "value": "morning"
                                }
                            },
                            {
                                "name": {
                                    "value": "in an hour"
                                }
                            },
                            {
                                "name": {
                                    "value": "afternoon"
                                }
                            },
                            {
                                "name": {
                                    "value": "evening"
                                }
                            },
                            {
                                "name": {
                                    "value": "tonight"
                                }
                            },
                            {
                                "name": {
                                    "value": "late night"
                                }
                            }
                        ],
                        "name": "contextualTime"
                    },
                    {
                        "values": [
                            {
                                "id": "spa",
                                "name": {
                                    "value": "spa"
                                }
                            },
                            {
                                "id": "birlaplanetarium",
                                "name": {
                                    "value": "birla planetarium",
                                    "synonyms": [
                                        "planetarium",
                                        "Birla Planetarium"
                                    ]
                                }
                            },
                            {
                                "id": "birlamandir",
                                "name": {
                                    "value": "birla mandir",
                                    "synonyms": [
                                        "birla temple",
                                        "birla mandir",
                                        "birla Mandir",
                                        "Birla Mandir",
                                        "mandir"
                                    ]
                                }
                            },
                            {
                                "id": "golcondafort",
                                "name": {
                                    "value": "golconda fort"
                                }
                            },
                            {
                                "id": "inorbitmall",
                                "name": {
                                    "value": "inorbit mall",
                                    "synonyms": [
                                        "orbit mall"
                                    ]
                                }
                            },
                            {
                                "id": "hicc",
                                "name": {
                                    "value": "hicc",
                                    "synonyms": [
                                        "hyderabad convention centre",
                                        "convention centre",
                                        "convnetion center",
                                        "hyderabad international convention center",
                                        "convention center"
                                    ]
                                }
                            },
                            {
                                "id": "botanicalgarden",
                                "name": {
                                    "value": "botanical garden"
                                }
                            },
                            {
                                "id": "zyng",
                                "name": {
                                    "value": "zyng"
                                }
                            },
                            {
                                "id": "theorientalblossom",
                                "name": {
                                    "value": "the oriental blossom",
                                    "synonyms": [
                                        "oriental blossom"
                                    ]
                                }
                            },
                            {
                                "id": "chamagoucha",
                                "name": {
                                    "value": "chama goucha"
                                }
                            },
                            {
                                "id": "cascade",
                                "name": {
                                    "value": "cascade"
                                }
                            },
                            {
                                "id": "gym",
                                "name": {
                                    "value": "gym"
                                }
                            }
                        ],
                        "name": "Facilities"
                    },
                    {
                        "values": [
                            {
                                "id": "DOORLOCK",
                                "name": {
                                    "value": "door lock",
                                    "synonyms": [
                                        "door is not opening",
                                        "door not opening"
                                    ]
                                }
                            },
                            {
                                "id": "WIFI",
                                "name": {
                                    "value": "wifi",
                                    "synonyms": [
                                        "wifi is slow",
                                        "WiFi is not working",
                                        "WiFi"
                                    ]
                                }
                            },
                            {
                                "id": "SOCKETS",
                                "name": {
                                    "value": "sockets",
                                    "synonyms": [
                                        "repair sockets",
                                        "repair power sockets",
                                        "power sockets are not working",
                                        "power socket is not working",
                                        "socket"
                                    ]
                                }
                            },
                            {
                                "id": "LIGHT",
                                "name": {
                                    "value": "light",
                                    "synonyms": [
                                        "lights flickering",
                                        "bedroom light is not working",
                                        "bathroom light is not working",
                                        "repair room lights",
                                        "repair bathroom lights",
                                        "repair my lights",
                                        "repair the lights",
                                        "light is not working",
                                        "bedlamp is not working",
                                        "Bedside table lamp fused",
                                        "Bedside lamp switch defective",
                                        "Bedside Lamp Not Working"
                                    ]
                                }
                            },
                            {
                                "id": "TELEPHONE",
                                "name": {
                                    "value": "telephone",
                                    "synonyms": [
                                        "repair telephone",
                                        "repair my telephone",
                                        "telephone is dead",
                                        "telephone not working",
                                        "telephone is not working",
                                        "Telephone button is not working",
                                        "Telephone buttons are not working",
                                        "Bathroom telephone is not working",
                                        "Bathroom telephone not working",
                                        "Bedside telephone is not working",
                                        "Bedside telephone not working"
                                    ]
                                }
                            },
                            {
                                "id": "BLINDNOTWORKING",
                                "name": {
                                    "value": "blind not working",
                                    "synonyms": [
                                        "how to operate blind",
                                        "bathroom glass",
                                        "window blind is not working",
                                        "window blinds are not working",
                                        "blinds are not working",
                                        "blind is not working"
                                    ]
                                }
                            },
                            {
                                "id": "BLOCKEDDRAINAGE",
                                "name": {
                                    "value": "blocked drainage",
                                    "synonyms": [
                                        "water blockage",
                                        "water blocked",
                                        "water not draining",
                                        "drainage not effective",
                                        "drainage is not effective",
                                        "drainage system blocked",
                                        "drainage system is blocked",
                                        "drainage is blocked"
                                    ]
                                }
                            },
                            {
                                "id": "HOTWATERISNOTCOMING",
                                "name": {
                                    "value": "hot water is not coming",
                                    "synonyms": [
                                        "hot water not coming"
                                    ]
                                }
                            },
                            {
                                "id": "TV",
                                "name": {
                                    "value": "TV",
                                    "synonyms": [
                                        "tv channels not coming",
                                        "tv signal not coming",
                                        "T. V. is not switching on",
                                        "repair my TV",
                                        "repair my T. V.",
                                        "T. V. is not responding",
                                        "tv's screen is not working",
                                        "tv's display is not working",
                                        "T. V. is not working",
                                        "T.V."
                                    ]
                                }
                            },
                            {
                                "id": "AC",
                                "name": {
                                    "value": "AC",
                                    "synonyms": [
                                        "repair my air conditioner",
                                        "repair air conditioner",
                                        "repair AC",
                                        "repair my AC",
                                        "AC is not cooling",
                                        "AC is not effective",
                                        "AC is not working",
                                        "AC not effective",
                                        "AC not cooling",
                                        "AC not working",
                                        "air conditioner",
                                        "air conditioning",
                                        "A/C thermostat not working",
                                        "A/c is not effective",
                                        "AC not cooling properly",
                                        "AC is not cooling properly",
                                        "AC is not working properly",
                                        "AC not working properly",
                                        "AC ineffective",
                                        "AC not effective",
                                        "AC is not effective",
                                        "AC",
                                        "air conditioner",
                                        "AC is not working",
                                        "AC not working",
                                        "AC is ineffective",
                                        "AC is not effective",
                                        "AC is not cooling",
                                        "repair my AC",
                                        "repair AC",
                                        "repair air conditioner",
                                        "repair my air conditioner",
                                        "A/C thermostat not working",
                                        "AC Noisy",
                                        "AC not effective",
                                        "AC not working",
                                        "AC thermostat display not working",
                                        "AC to Switch OFF",
                                        "AC to Switch ON",
                                        "AC Too Cold",
                                        "AC Too Hot",
                                        "Bed room AC Noisy",
                                        "Bed room AC not effective",
                                        "Bed room AC not working",
                                        "Bed room AC thermostat display crack",
                                        "Bed room AC thermostat display not working",
                                        "Bed room AC Too Cold",
                                        "Bed room AC Too Hot",
                                        "Living room AC noisy",
                                        "Living room AC not effective",
                                        "Living room AC not working",
                                        "Temperature is high",
                                        "Temperature is low",
                                        "Temperature Not Adequate",
                                        "Temperature is high"
                                    ]
                                }
                            },
                            {
                                "id": "REFRIGERATOR",
                                "name": {
                                    "value": "Refrigerator",
                                    "synonyms": [
                                        "repair refrigerator",
                                        "repair mini bar",
                                        "repair my mini bar",
                                        "repair my refrigerator",
                                        "mini bar not cooling",
                                        "fridge",
                                        "mini fridge",
                                        "mini bar",
                                        "bar"
                                    ]
                                }
                            },
                            {
                                "id": "LOCKER",
                                "name": {
                                    "value": "Locker",
                                    "synonyms": [
                                        "el safe lock",
                                        "el safe",
                                        "repair locker",
                                        "repair my locker"
                                    ]
                                }
                            },
                            {
                                "id": "FLUSH",
                                "name": {
                                    "value": "Flush",
                                    "synonyms": [
                                        "toilet flush",
                                        "toilet"
                                    ]
                                }
                            },
                            {
                                "id": "TVREMOTE",
                                "name": {
                                    "value": "TVRemote",
                                    "synonyms": [
                                        "TV remote is not responding",
                                        "repair my TV remote",
                                        "TV remote is not working",
                                        "TV remote"
                                    ]
                                }
                            }
                        ],
                        "name": "Complaint"
                    },
                    {
                        "values": [
                            {
                                "id": "WASH",
                                "name": {
                                    "value": "washclothes",
                                    "synonyms": [
                                        "folding",
                                        "fold",
                                        "drying",
                                        "dry",
                                        "rinsing",
                                        "rinse",
                                        "washing",
                                        "laundry"
                                    ]
                                }
                            },
                            {
                                "id": "IRON",
                                "name": {
                                    "value": "ironpress",
                                    "synonyms": [
                                        "iron press"
                                    ]
                                }
                            },
                            {
                                "id": "STEAM",
                                "name": {
                                    "value": "steampress",
                                    "synonyms": [
                                        "steam press",
                                        "steam iron"
                                    ]
                                }
                            }
                        ],
                        "name": "laundry"
                    },
                    {
                        "values": [
                            {
                                "id": "BUSSTOP",
                                "name": {
                                    "value": "BusStop",
                                    "synonyms": [
                                        "Bus stand",
                                        "bus stop"
                                    ]
                                }
                            },
                            {
                                "id": "AIRPORT",
                                "name": {
                                    "value": "Airport"
                                }
                            },
                            {
                                "id": "RAILWAY",
                                "name": {
                                    "value": "RailwayStation",
                                    "synonyms": [
                                        "railway station"
                                    ]
                                }
                            }
                        ],
                        "name": "Place"
                    },
                    {
                        "values": [
                            {
                                "name": {
                                    "value": "foodOrder"
                                }
                            },
                            {
                                "name": {
                                    "value": "bookSpa"
                                }
                            },
                            {
                                "name": {
                                    "value": "bookTable"
                                }
                            }
                        ],
                        "name": "request"
                    },
                    {
                        "values": [
                            {
                                "id": "CONCIERGE",
                                "name": {
                                    "value": "concierge",
                                    "synonyms": [
                                        "car",
                                        "concierge",
                                        "cab"
                                    ]
                                }
                            },
                            {
                                "id": "SERVICES",
                                "name": {
                                    "value": "Services",
                                    "synonyms": [
                                        "door is not opening",
                                        "door not opening\t",
                                        "AC complaint",
                                        "TV complaint",
                                        "tv channels not coming",
                                        "tv signal not coming",
                                        "T. V. is not switching on",
                                        "repair my TV",
                                        "repair my T. V.",
                                        "T. V. is not responding",
                                        "tv's screen is not working",
                                        "tv's display is not working",
                                        "T. V. is not working",
                                        "T.V.",
                                        "Remote complaint",
                                        "TV Remote complaint",
                                        "TV remote is not responding",
                                        "repair my TV remote",
                                        "TV remote is not working",
                                        "TV remote",
                                        "repair my air conditioner",
                                        "repair air conditioner",
                                        "repair AC",
                                        "repair my AC",
                                        "AC is not cooling",
                                        "AC is not effective",
                                        "AC is not working",
                                        "AC not effective",
                                        "AC not cooling",
                                        "AC not working",
                                        "air conditioner",
                                        "air conditioning",
                                        "A/C thermostat not working",
                                        "A/c is not effective",
                                        "AC not cooling properly",
                                        "AC is not cooling properly",
                                        "AC is not working properly",
                                        "AC not working properly",
                                        "AC ineffective",
                                        "AC not effective",
                                        "AC is not effective",
                                        "AC",
                                        "air conditioner",
                                        "AC is not working",
                                        "AC not working",
                                        "AC is ineffective",
                                        "AC is not effective",
                                        "AC is not cooling",
                                        "repair my AC",
                                        "repair AC",
                                        "repair air conditioner",
                                        "repair my air conditioner",
                                        "A/C thermostat not working",
                                        "AC Noisy",
                                        "AC not effective",
                                        "AC not working",
                                        "AC thermostat display not working",
                                        "AC to Switch OFF",
                                        "AC to Switch ON",
                                        "AC Too Cold",
                                        "AC Too Hot",
                                        "Bed room AC Noisy",
                                        "Bed room AC not effective",
                                        "Bed room AC not working",
                                        "Bed room AC thermostat display crack",
                                        "Bed room AC thermostat display not working",
                                        "Bed room AC Too Cold",
                                        "Bed room AC Too Hot",
                                        "Living room AC noisy",
                                        "Living room AC not effective",
                                        "Living room AC not working",
                                        "Temperature is high",
                                        "refrigerator complaint",
                                        "repair refrigerator",
                                        "repair mini bar",
                                        "repair my mini bar",
                                        "repair my refrigerator",
                                        "mini bar not cooling",
                                        "fridge",
                                        "mini fridge",
                                        "mini bar",
                                        "bar",
                                        "locker complaint",
                                        "el safe lock",
                                        "el safe",
                                        "repair locker",
                                        "repair my locker",
                                        "Temperature is low",
                                        "Flush complaint",
                                        "toilet flush",
                                        "toilet",
                                        "lights flickering",
                                        "bedroom light is not working",
                                        "bathroom light is not working",
                                        "repair room lights",
                                        "repair bathroom lights",
                                        "repair my lights",
                                        "repair the lights",
                                        "light is not working",
                                        "bedlamp is not working",
                                        "Bedside table lamp fused",
                                        "Bedside lamp switch defective",
                                        "Bedside Lamp Not Working",
                                        "repair telephone",
                                        "repair my telephone",
                                        "telephone is dead",
                                        "telephone not working",
                                        "telephone is not working",
                                        "Telephone button is not working",
                                        "Telephone buttons are not working",
                                        "Bathroom telephone is not working",
                                        "Bathroom telephone not working",
                                        "Bedside telephone is not working",
                                        "Bedside telephone not working",
                                        "wifi is slow",
                                        "WiFi is not working",
                                        "WiFi",
                                        "repair sockets",
                                        "repair power sockets",
                                        "power sockets are not working",
                                        "power socket is not working",
                                        "socket",
                                        "how to operate blind",
                                        "bathroom glass",
                                        "window blind is not working",
                                        "window blinds are not working",
                                        "blinds are not working",
                                        "blind is not working",
                                        "water blockage",
                                        "water blocked",
                                        "water not draining",
                                        "drainage not effective",
                                        "drainage is not effective",
                                        "drainage system blocked",
                                        "drainage system is blocked",
                                        "drainage is blocked",
                                        "hot water not coming",
                                        "Temperature Not Adequate",
                                        "Temperature is high"
                                    ]
                                }
                            },
                            {
                                "id": "AMENITIES",
                                "name": {
                                    "value": "AMENITIES",
                                    "synonyms": [
                                        "toothbrush",
                                        "tooth brush",
                                        "soap bar",
                                        "bar",
                                        "soaps",
                                        "towaliya",
                                        "shower gel",
                                        "bath robes",
                                        "bathrobes",
                                        "bathrobe",
                                        "bath robe",
                                        "tissues",
                                        "tissue",
                                        "laundry bags",
                                        "combs",
                                        "comb",
                                        "shower cap",
                                        "toilet tissues",
                                        "facial tissues",
                                        "facial tissue",
                                        "face tissue",
                                        "toilet tissue",
                                        "shaving kit",
                                        "dental kit",
                                        "toiletries",
                                        "toilet paper",
                                        "body moisturizer",
                                        "body lotion",
                                        "moisturizer",
                                        "shower cap",
                                        "shower caps",
                                        "sanitary bag",
                                        "sanitary bags",
                                        "Shaving kit",
                                        "dental kit",
                                        "hair conditioner",
                                        "conditioner",
                                        "Body cleanser",
                                        "hair cleanser",
                                        "hair brush",
                                        "comb",
                                        "laundry bag",
                                        "shampoo",
                                        "soap",
                                        "cotton buds",
                                        "mouth wash",
                                        "toothpaste",
                                        "after Shave",
                                        "shaving cream",
                                        "razor",
                                        "Shaver",
                                        "collin glass",
                                        "collin glasses",
                                        "high ball glasses",
                                        "high ball glass",
                                        "cups",
                                        "barf",
                                        "baraf",
                                        "stirrer",
                                        "ice",
                                        "ice bucket",
                                        "ice cubes",
                                        "tea spoons",
                                        "tea spoon",
                                        "toothpick",
                                        "cups",
                                        "spoon",
                                        "spoons",
                                        "chammach",
                                        "katori",
                                        "bowl",
                                        "bowls",
                                        "spoon",
                                        "spoons",
                                        "knife",
                                        "fork",
                                        "forks",
                                        "cutlery",
                                        "wine glasses",
                                        "wine glass",
                                        "cocktail glass",
                                        "cocktail glasses",
                                        "shot glasses",
                                        "shot glass",
                                        "whiskey glasses",
                                        "whiskey glass",
                                        "glass",
                                        "glasses",
                                        "goblet",
                                        "goblets",
                                        "plates",
                                        "plate",
                                        "toli yeah",
                                        "toliyeah",
                                        "bath mats",
                                        "yoga mat",
                                        "Yoga mat",
                                        "bath floor mat",
                                        "Bath floor Mat",
                                        "bath rob",
                                        "Bath Rob",
                                        "hand towel",
                                        "hand towels",
                                        "bath towel",
                                        "bath towels",
                                        "face towel",
                                        "face towels",
                                        "towel",
                                        "towels",
                                        "bathrobe",
                                        "bath mat",
                                        "bathrobes",
                                        "blankets",
                                        "pillows",
                                        "pillow covers",
                                        "sheets",
                                        "baby cot",
                                        "cot",
                                        "linen",
                                        "extra bed",
                                        "bedsheet",
                                        "bedding",
                                        "bed sheets",
                                        "blanket",
                                        "blankets",
                                        "bedsheets",
                                        "mattress",
                                        "soft pillows",
                                        "hard pillows",
                                        "extra pillow",
                                        "extra pillows",
                                        "pillows",
                                        "Bisleri water",
                                        "RO water",
                                        "RO water bottles",
                                        "Bisleri water bottle",
                                        "RO water bottle",
                                        "water bottle",
                                        "water bottles",
                                        "bottle",
                                        "bottles",
                                        "waterbottles",
                                        "milk powder sachet",
                                        "milk powder sachets",
                                        "carry bags",
                                        "polybags",
                                        "Polythene",
                                        "soft drinks",
                                        "aam panna",
                                        "almonds",
                                        "cashew nuts",
                                        "digestive biscuits",
                                        "kitkat",
                                        "hide  and seek",
                                        "lays",
                                        "minibar items",
                                        "bottle opener",
                                        "stationary",
                                        "pen",
                                        "pencil",
                                        "notepads",
                                        "buttons",
                                        "thread",
                                        "needle",
                                        "cups cleaning",
                                        "cups cleanings",
                                        "kettle cleaning",
                                        "milk powder",
                                        "milk sachets",
                                        "creamer",
                                        "milk creamer",
                                        "green tea",
                                        "sugarfree",
                                        "equal",
                                        "brown sugar",
                                        "white sugar",
                                        "sugar",
                                        "english breakfast",
                                        "darjeeling tea",
                                        "coffee",
                                        "Tea",
                                        "disposable bags",
                                        "disposable bag",
                                        "bandages",
                                        "bandage",
                                        "earbud",
                                        "earbuds",
                                        "vanity set",
                                        "hangers",
                                        "chair",
                                        "chairs",
                                        "extra chairs",
                                        "extra chair",
                                        "chair",
                                        "bucket and mug",
                                        "mug",
                                        "bucket",
                                        "hangers",
                                        "minibar items",
                                        "iron box",
                                        "iron board and box",
                                        "slippers",
                                        "scissor",
                                        "scissors",
                                        "tcm supplies",
                                        "TCM supplies",
                                        "nail cutter",
                                        "laundry slips",
                                        "laundry slip",
                                        "iron board",
                                        "electric iron",
                                        "flat iron",
                                        "iron",
                                        "smooth iron",
                                        "steam iron",
                                        "sewing kit"
                                    ]
                                }
                            },
                            {
                                "id": "LAUNDRY",
                                "name": {
                                    "value": "laundry",
                                    "synonyms": [
                                        "clothes"
                                    ]
                                }
                            },
                            {
                                "id": "FOODORDER",
                                "name": {
                                    "value": "foodOrder",
                                    "synonyms": [
                                        "food order",
                                        "food"
                                    ]
                                }
                            },
                            {
                                "id": "BOOKSPA",
                                "name": {
                                    "value": "bookspa",
                                    "synonyms": [
                                        "book spa",
                                        "SPA booking",
                                        "spa booking",
                                        "spa"
                                    ]
                                }
                            },
                            {
                                "id": "BOOKTABLE",
                                "name": {
                                    "value": "booktable",
                                    "synonyms": [
                                        "book table",
                                        "table booking",
                                        "table"
                                    ]
                                }
                            },
                            {
                                "id": "CONNECTRECEPTION",
                                "name": {
                                    "value": "connectFrontdesk",
                                    "synonyms": [
                                        "call reception",
                                        "connect reception",
                                        "connect to reception",
                                        "connect to frontdesk",
                                        "connect frontdesk",
                                        "call frontdesk"
                                    ]
                                }
                            },
                            {
                                "id": "VALETPARKING",
                                "name": {
                                    "value": "valetParking",
                                    "synonyms": [
                                        "valet",
                                        "valet parking",
                                        "car"
                                    ]
                                }
                            },
                            {
                                "id": "EXTENDEDCHECKOUT",
                                "name": {
                                    "value": "ExtendedCheckOut",
                                    "synonyms": [
                                        "extended check out",
                                        "extended check"
                                    ]
                                }
                            }
                        ],
                        "name": "requeststatus"
                    },
                    {
                        "values": [
                            {
                                "id": "SANITIZER",
                                "name": {
                                    "value": "Sanitizer",
                                    "synonyms": [
                                        "splenda",
                                        "honey",
                                        "lip balm",
                                        "floss",
                                        "body cleanser",
                                        "bodywash",
                                        "handwash",
                                        "towelettes",
                                        "blow dryer",
                                        "hair curler",
                                        "hair straightener",
                                        "cushions",
                                        "sanitizer",
                                        "face cleanser",
                                        "deodorant",
                                        "deo",
                                        "perfume"
                                    ]
                                }
                            },
                            {
                                "id": "TOILETRIES",
                                "name": {
                                    "value": "toiletries",
                                    "synonyms": [
                                        "toothbrush",
                                        "tooth brush",
                                        "soap bar",
                                        "bar",
                                        "soaps",
                                        "towaliya",
                                        "shower gel",
                                        "bath robes",
                                        "bathrobes",
                                        "bathrobe",
                                        "bath robe",
                                        "tissues",
                                        "tissue",
                                        "laundry bags",
                                        "combs",
                                        "comb",
                                        "shower cap",
                                        "toilet tissues",
                                        "facial tissues",
                                        "facial tissue",
                                        "face tissue",
                                        "toilet tissue",
                                        "shaving kit",
                                        "dental kit",
                                        "toiletries",
                                        "toilet paper",
                                        "body moisturizer",
                                        "body lotion",
                                        "moisturizer",
                                        "shower cap",
                                        "shower caps",
                                        "sanitary bag",
                                        "sanitary bags",
                                        "Shaving kit",
                                        "dental kit",
                                        "hair conditioner",
                                        "conditioner",
                                        "Body cleanser",
                                        "hair cleanser",
                                        "hair brush",
                                        "comb",
                                        "laundry bag",
                                        "shampoo",
                                        "soap",
                                        "cotton buds",
                                        "mouth wash",
                                        "toothpaste",
                                        "after Shave",
                                        "shaving cream",
                                        "razor",
                                        "Shaver"
                                    ]
                                }
                            },
                            {
                                "id": "CUTLERY",
                                "name": {
                                    "value": "cutlery",
                                    "synonyms": [
                                        "collin glass",
                                        "collin glasses",
                                        "high ball glasses",
                                        "high ball glass",
                                        "cups",
                                        "barf",
                                        "baraf",
                                        "stirrer",
                                        "ice",
                                        "ice bucket",
                                        "ice cubes",
                                        "tea spoons",
                                        "tea spoon",
                                        "toothpick",
                                        "cups",
                                        "spoon",
                                        "spoons",
                                        "chammach",
                                        "katori",
                                        "bowl",
                                        "bowls",
                                        "spoon",
                                        "spoons",
                                        "knife",
                                        "fork",
                                        "forks",
                                        "cutlery",
                                        "wine glasses",
                                        "wine glass",
                                        "cocktail glass",
                                        "cocktail glasses",
                                        "shot glasses",
                                        "shot glass",
                                        "whiskey glasses",
                                        "whiskey glass",
                                        "glass",
                                        "glasses",
                                        "goblet",
                                        "goblets",
                                        "plates",
                                        "plate"
                                    ]
                                }
                            },
                            {
                                "id": "TOWELS",
                                "name": {
                                    "value": "Towels",
                                    "synonyms": [
                                        "toli yeah",
                                        "toliyeah",
                                        "bath mats",
                                        "yoga mat",
                                        "Yoga mat",
                                        "bath floor mat",
                                        "Bath floor Mat",
                                        "bath rob",
                                        "Bath Rob",
                                        "hand towel",
                                        "hand towels",
                                        "bath towel",
                                        "bath towels",
                                        "face towel",
                                        "face towels",
                                        "towel",
                                        "towels",
                                        "bathrobe",
                                        "bath mat",
                                        "bathrobes"
                                    ]
                                }
                            },
                            {
                                "id": "MATTRESS",
                                "name": {
                                    "value": "Mattress",
                                    "synonyms": [
                                        "blankets",
                                        "pillows",
                                        "pillow covers",
                                        "sheets",
                                        "baby cot",
                                        "cot",
                                        "linen",
                                        "extra bed",
                                        "bedsheet",
                                        "bedding",
                                        "bed sheets",
                                        "blanket",
                                        "blankets",
                                        "bedsheets"
                                    ]
                                }
                            },
                            {
                                "id": "PILLOW",
                                "name": {
                                    "value": "pillow",
                                    "synonyms": [
                                        "soft pillows",
                                        "hard pillows",
                                        "extra pillow",
                                        "extra pillows",
                                        "pillows"
                                    ]
                                }
                            },
                            {
                                "id": "WATERBOTTLE",
                                "name": {
                                    "value": "waterbottle",
                                    "synonyms": [
                                        "Bisleri water",
                                        "RO water",
                                        "RO water bottles",
                                        "Bisleri water bottle",
                                        "RO water bottle",
                                        "water bottle",
                                        "water bottles",
                                        "bottle",
                                        "bottles",
                                        "waterbottles"
                                    ]
                                }
                            },
                            {
                                "id": "MISC",
                                "name": {
                                    "value": "miscellaneous",
                                    "synonyms": [
                                        "scribbing pad",
                                        "milk powder sachet",
                                        "milk powder sachets",
                                        "carry bags",
                                        "polybags",
                                        "Polythene",
                                        "soft drinks",
                                        "aam panna",
                                        "almonds",
                                        "cashew nuts",
                                        "digestive biscuits",
                                        "kitkat",
                                        "hide  and seek",
                                        "lays",
                                        "minibar items",
                                        "bottle opener",
                                        "stationary",
                                        "pen",
                                        "pencil",
                                        "notepads",
                                        "buttons",
                                        "thread",
                                        "needle",
                                        "cups cleaning",
                                        "cups cleanings",
                                        "kettle cleaning",
                                        "milk powder",
                                        "milk sachets",
                                        "creamer",
                                        "milk creamer",
                                        "green tea",
                                        "sugarfree",
                                        "equal",
                                        "brown sugar",
                                        "white sugar",
                                        "sugar",
                                        "english breakfast",
                                        "darjeeling tea",
                                        "coffee",
                                        "Tea",
                                        "disposable bags",
                                        "disposable bag",
                                        "bandages",
                                        "bandage",
                                        "earbud",
                                        "earbuds",
                                        "vanity set",
                                        "hangers",
                                        "chair",
                                        "chairs",
                                        "extra chairs",
                                        "extra chair",
                                        "chair",
                                        "bucket and mug",
                                        "mug",
                                        "bucket",
                                        "hangers",
                                        "minibar items",
                                        "iron box",
                                        "iron board and box",
                                        "slippers",
                                        "scissor",
                                        "scissors",
                                        "tcm supplies",
                                        "TCM supplies",
                                        "nail cutter",
                                        "laundry slips",
                                        "laundry slip",
                                        "iron board",
                                        "electric iron",
                                        "flat iron",
                                        "iron",
                                        "smooth iron",
                                        "steam iron",
                                        "sewing kit"
                                    ]
                                }
                            }
                        ],
                        "name": "Toiletries"
                    },
                    {
                        "values": [
                            {
                                "id": "RAILWAY",
                                "name": {
                                    "value": "railway station"
                                }
                            },
                            {
                                "id": "BUS",
                                "name": {
                                    "value": "bus stop",
                                    "synonyms": [
                                        "bus stand"
                                    ]
                                }
                            },
                            {
                                "id": "AIRPORT",
                                "name": {
                                    "value": "airport"
                                }
                            }
                        ],
                        "name": "destination"
                    },
                    {
                        "values": [
                            {
                                "name": {
                                    "value": "TheOrientalBlossom"
                                }
                            },
                            {
                                "name": {
                                    "value": "HornOkPlease"
                                }
                            },
                            {
                                "name": {
                                    "value": "ChamaGaucha"
                                }
                            }
                        ],
                        "name": "restaurant"
                    },
                    {
                        "values": [
                            {
                                "name": {
                                    "value": "No"
                                }
                            },
                            {
                                "name": {
                                    "value": "Yes"
                                }
                            }
                        ],
                        "name": "YesOrNo"
                    },
                    {
                        "values": [
                            {
                                "name": {
                                    "value": "sure"
                                }
                            },
                            {
                                "name": {
                                    "value": "no"
                                }
                            },
                            {
                                "name": {
                                    "value": "yes"
                                }
                            },
                            {
                                "name": {
                                    "value": "Coffee"
                                }
                            },
                            {
                                "name": {
                                    "value": "Tea"
                                }
                            }
                        ],
                        "name": "TeaCoffee"
                    },
                    {
                        "values": [
                            {
                                "name": {
                                    "value": "DND"
                                }
                            },
                            {
                                "id": "COTTONBALL",
                                "name": {
                                    "value": "Cotton Balls",
                                    "synonyms": [
                                        "cotton ball"
                                    ]
                                }
                            },
                            {
                                "id": "ICEBAG",
                                "name": {
                                    "value": "Ice bag",
                                    "synonyms": [
                                        "ice bag",
                                        "ice bags"
                                    ]
                                }
                            },
                            {
                                "id": "HOTWATERBAG",
                                "name": {
                                    "value": "Hot water bag",
                                    "synonyms": [
                                        "hot water bag",
                                        "hot water bags"
                                    ]
                                }
                            },
                            {
                                "id": "SHOESHINNER",
                                "name": {
                                    "value": "Shoe Shinner",
                                    "synonyms": [
                                        "shoe polish",
                                        "shoe shinners"
                                    ]
                                }
                            },
                            {
                                "id": "CHANGEBEDSHEET",
                                "name": {
                                    "value": "ChangeMyBedsheet",
                                    "synonyms": [
                                        "change my sheets",
                                        "change sheets",
                                        "change bedsheet",
                                        "change my bedsheet"
                                    ]
                                }
                            },
                            {
                                "id": "HOUSEKEEPING",
                                "name": {
                                    "value": "Housekeeping",
                                    "synonyms": [
                                        "house keeping",
                                        "housekeeping"
                                    ]
                                }
                            },
                            {
                                "id": "CLEAN",
                                "name": {
                                    "value": "Clean",
                                    "synonyms": [
                                        "replenishment",
                                        "basin cleaning",
                                        "Shower cleaning",
                                        "WC cleaning",
                                        "bathroom cleaning",
                                        "dustbin clearance",
                                        "carpet cleaning",
                                        "room cleaning",
                                        "cleaning"
                                    ]
                                }
                            },
                            {
                                "id": "TEABAGS",
                                "name": {
                                    "value": "teabags",
                                    "synonyms": [
                                        "coffee amenities",
                                        "coffee amenity",
                                        "tea amenities",
                                        "tea amenity",
                                        "green tea",
                                        "tea",
                                        "tea bags",
                                        "tea bag"
                                    ]
                                }
                            },
                            {
                                "id": "NEWSPAPERS",
                                "name": {
                                    "value": "newspapers",
                                    "synonyms": [
                                        "paper",
                                        "newspaper",
                                        "newspapers"
                                    ]
                                }
                            },
                            {
                                "id": "ROOMHEATER",
                                "name": {
                                    "value": "RoomHeater",
                                    "synonyms": [
                                        "room heater"
                                    ]
                                }
                            },
                            {
                                "id": "BATHTUB",
                                "name": {
                                    "value": "BathTub",
                                    "synonyms": [
                                        "bath tub"
                                    ]
                                }
                            },
                            {
                                "id": "BATHROOMCLEAN",
                                "name": {
                                    "value": "BathroomClean",
                                    "synonyms": [
                                        "clean my bathroom ",
                                        "bathroom clean",
                                        "cleaning bathroom",
                                        "bathroom cleaning"
                                    ]
                                }
                            },
                            {
                                "id": "ROOMCLEAN",
                                "name": {
                                    "value": "RoomClean",
                                    "synonyms": [
                                        "room cleaning",
                                        "cleaning room",
                                        "room clean"
                                    ]
                                }
                            },
                            {
                                "id": "CHANGE",
                                "name": {
                                    "value": "change"
                                }
                            }
                        ],
                        "name": "Housekeeping"
                    },
                ]
            },
            "dialog": {
                "intents": [
                    {
                        "name": "AmenitiesIntent",
                        "confirmationRequired": false,
                        "prompts": {},
                        "slots": [
                            {
                                "name": "Toiletries",
                                "type": "Toiletries",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Intent-AmenitiesIntent.IntentSlot-Toiletries"
                                }
                            },
                            {
                                "name": "Number",
                                "type": "AMAZON.NUMBER",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Intent-AmenitiesIntent.IntentSlot-Number"
                                }
                            }
                        ]
                    },
                    {
                        "name": "BookSpaIntent",
                        "confirmationRequired": true,
                        "prompts": {
                            "confirmation": "Confirm.Intent-BookSpaIntent"
                        },
                        "slots": [
                            {
                                "name": "Date",
                                "type": "AMAZON.DATE",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Intent-BookSpaIntent.IntentSlot-Date"
                                }
                            },
                            {
                                "name": "Time",
                                "type": "AMAZON.TIME",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Intent-BookSpaIntent.IntentSlot-Time"
                                }
                            },
                            {
                                "name": "Number",
                                "type": "AMAZON.NUMBER",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Intent-BookSpaIntent.IntentSlot-Number"
                                }
                            }
                        ]
                    },
                    {
                        "name": "BookTableIntent",
                        "confirmationRequired": true,
                        "prompts": {
                            "confirmation": "Confirm.Intent.48608787701"
                        },
                        "slots": [
                            {
                                "name": "Number",
                                "type": "AMAZON.NUMBER",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Intent-BookTableIntent.IntentSlot-Number"
                                }
                            },
                            {
                                "name": "date",
                                "type": "AMAZON.DATE",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Intent-BookTableIntent.IntentSlot-date"
                                }
                            },
                            {
                                "name": "time",
                                "type": "AMAZON.TIME",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Intent-BookTableIntent.IntentSlot-time"
                                }
                            },
                            {
                                "name": "restaurant",
                                "type": "restaurant",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.1446900248282.1034572689561"
                                }
                            }
                        ]
                    },
                    {
                        "name": "ExtendedCheckOutIntent",
                        "confirmationRequired": false,
                        "prompts": {},
                        "slots": [
                            {
                                "name": "Time",
                                "type": "AMAZON.TIME",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Intent-ExtendedCheckOutIntent.IntentSlot-Time"
                                }
                            }
                        ]
                    },
                    {
                        "name": "SelectIntent",
                        "confirmationRequired": false,
                        "prompts": {},
                        "slots": [
                            {
                                "name": "number",
                                "type": "AMAZON.NUMBER",
                                "elicitationRequired": false,
                                "confirmationRequired": false,
                                "prompts": {}
                            },
                            {
                                "name": "quantity",
                                "type": "AMAZON.NUMBER",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.1492880162469.1383306652241"
                                }
                            }
                        ]
                    },
                    {
                        "name": "ConciergeIntent",
                        "confirmationRequired": true,
                        "prompts": {
                            "confirmation": "Confirm.Intent.696790494114"
                        },
                        "slots": [
                            {
                                "name": "Destination",
                                "type": "destination",
                                "elicitationRequired": false,
                                "confirmationRequired": false,
                                "prompts": {}
                            },
                            {
                                "name": "time",
                                "type": "AMAZON.TIME",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.1201643730418.378245265682"
                                }
                            },
                            {
                                "name": "date",
                                "type": "AMAZON.DATE",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.1201643730418.1455008076377"
                                }
                            }
                        ]
                    },
                    {
                        "name": "BellBoyIntent",
                        "confirmationRequired": true,
                        "prompts": {
                            "confirmation": "Confirm.Intent.800837118355"
                        },
                        "slots": [
                            {
                                "name": "luggage",
                                "type": "AMAZON.NUMBER",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.803941735269.1406019987826"
                                }
                            }
                        ]
                    },
                    {
                        "name": "ConnectFrontdeskIntent",
                        "confirmationRequired": true,
                        "prompts": {
                            "confirmation": "Confirm.Intent.1448742181414"
                        },
                        "slots": []
                    },
                    {
                        "name": "LaundryIntent",
                        "confirmationRequired": false,
                        "prompts": {},
                        "slots": [
                            {
                                "name": "laundry",
                                "type": "laundry",
                                "elicitationRequired": false,
                                "confirmationRequired": false,
                                "prompts": {}
                            },
                            {
                                "name": "clothes",
                                "type": "clothes",
                                "elicitationRequired": false,
                                "confirmationRequired": false,
                                "prompts": {}
                            }
                        ]
                    },
                    {
                        "name": "ServicesIntent",
                        "confirmationRequired": true,
                        "prompts": {
                            "confirmation": "Confirm.Intent.881023804387"
                        },
                        "slots": [
                            {
                                "name": "Complaints",
                                "type": "Complaint",
                                "elicitationRequired": false,
                                "confirmationRequired": false,
                                "prompts": {}
                            }
                        ]
                    },
                    {
                        "name": "ValetParkingIntent",
                        "confirmationRequired": true,
                        "prompts": {
                            "confirmation": "Confirm.Intent.328632889962"
                        },
                        "slots": []
                    },
                    {
                        "name": "WakeUpCallIntent",
                        "confirmationRequired": false,
                        "prompts": {},
                        "slots": [
                            {
                                "name": "Time",
                                "type": "AMAZON.TIME",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.75540223438.1174920170145"
                                }
                            },
                            {
                                "name": "Date",
                                "type": "AMAZON.DATE",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.75540223438.1431178814880"
                                }
                            },
                            {
                                "name": "SecondWakeCall",
                                "type": "YesOrNo",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.724436750957.193217565017"
                                }
                            },
                            {
                                "name": "TeaCoffee",
                                "type": "TeaCoffee",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.950778260332.1063987566731"
                                }
                            }
                        ]
                    },
                    {
                        "name": "AvailabilityIntent",
                        "confirmationRequired": false,
                        "prompts": {},
                        "slots": [
                            {
                                "name": "Toiletries",
                                "type": "Toiletries",
                                "elicitationRequired": false,
                                "confirmationRequired": false,
                                "prompts": {}
                            },
                            {
                                "name": "housekeeping",
                                "type": "Housekeeping",
                                "elicitationRequired": false,
                                "confirmationRequired": false,
                                "prompts": {}
                            }
                        ]
                    },
                    {
                        "name": "HousekeepingIntent",
                        "confirmationRequired": false,
                        "prompts": {},
                        "slots": [
                            {
                                "name": "Housekeeping",
                                "type": "Housekeeping",
                                "elicitationRequired": false,
                                "confirmationRequired": false,
                                "prompts": {}
                            }
                        ]
                    },
                    {
                        "name": "NearbyCabIntent",
                        "confirmationRequired": false,
                        "prompts": {},
                        "slots": [
                            {
                                "name": "Time",
                                "type": "AMAZON.TIME",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.623034417450.1011341634024"
                                }
                            },
                            {
                                "name": "Date",
                                "type": "AMAZON.DATE",
                                "elicitationRequired": true,
                                "confirmationRequired": false,
                                "prompts": {
                                    "elicitation": "Elicit.Slot.623034417450.714983784085"
                                }
                            }
                        ]
                    }
                ],
                "delegationStrategy": "SKILL_RESPONSE"
            },
            "prompts": [
                {
                    "id": "Elicit.Intent-AmenitiesIntent.IntentSlot-Toiletries",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "what amenity would you like to order?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-AmenitiesIntent.IntentSlot-Number",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "How many {Toiletries}"
                        },
                        {
                            "type": "PlainText",
                            "value": "how many {Toiletries} you need"
                        },
                        {
                            "type": "PlainText",
                            "value": "okay. how many {Toiletries}"
                        },
                        {
                            "type": "PlainText",
                            "value": "okay. how many {Toiletries} would you like to order"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent-BookSpaIntent",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "would you like to confirm your spa booking for {Number} people on {Date} at {Time}"
                        },
                        {
                            "type": "PlainText",
                            "value": "do you want to confirm your spa booking?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-BookSpaIntent.IntentSlot-Date",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "and on what day do you want your booking for"
                        },
                        {
                            "type": "PlainText",
                            "value": "sure. what day would you like your booking to be"
                        },
                        {
                            "type": "PlainText",
                            "value": "what day would you like your booking to be"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-BookSpaIntent.IntentSlot-Time",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "and at what time you want your booking slot for"
                        },
                        {
                            "type": "PlainText",
                            "value": "at what time would you like to book a slot"
                        },
                        {
                            "type": "PlainText",
                            "value": "at what time you would like your booking to be"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-BookSpaIntent.IntentSlot-Number",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "sure. For how many people"
                        },
                        {
                            "type": "PlainText",
                            "value": "and for how many people"
                        },
                        {
                            "type": "PlainText",
                            "value": "great. for how many people"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-BookTableIntent.IntentSlot-Number",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "for how many people would you like to book the table"
                        },
                        {
                            "type": "PlainText",
                            "value": "great. for how many people would you like to book the table"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-BookTableIntent.IntentSlot-date",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "and for when do you want your booking for?"
                        },
                        {
                            "type": "PlainText",
                            "value": "great. when would you like your booking to be"
                        },
                        {
                            "type": "PlainText",
                            "value": "great. when would you like your booking for"
                        },
                        {
                            "type": "PlainText",
                            "value": "okay. For when would you like your booking to be"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-BookTableIntent.IntentSlot-time",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "at what time do you want your booking for"
                        },
                        {
                            "type": "PlainText",
                            "value": "at what time would you like your booking to be"
                        },
                        {
                            "type": "PlainText",
                            "value": "at what time would you like your booking for"
                        },
                        {
                            "type": "PlainText",
                            "value": "and at what time would you like your booking to be"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-ChangeFoodIntent.IntentSlot-fromFood",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "which food item would you like to change"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-ChangeFoodIntent.IntentSlot-toQuant",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "how many would you like to change it to?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-DeliciousIntent.IntentSlot-Numb",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "for how many?"
                        },
                        {
                            "type": "PlainText",
                            "value": "for how many people would you like to order"
                        },
                        {
                            "type": "PlainText",
                            "value": "and for how many people would you like to order"
                        },
                        {
                            "type": "PlainText",
                            "value": "okay. and for how many people would you like to order"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-DeliciousIntent.IntentSlot-Meal",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "you want to have breakfast, lunch or dinner?"
                        },
                        {
                            "type": "PlainText",
                            "value": "what would you like to have breakfast,lunch or dinner"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-DeliciousIntent.IntentSlot-type",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "sure veg or non veg, please specify"
                        },
                        {
                            "type": "PlainText",
                            "value": "okay you want to have veg or no veg?"
                        },
                        {
                            "type": "PlainText",
                            "value": "okay. vegetarian or non vegetarian. please specify"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-ExtendedCheckOutIntent.IntentSlot-Time",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "at what time would you like to check out"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-RemoveFoodOrderIntent.IntentSlot-Food",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "Which food item would you like to remove?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Intent-ScheduleFoodOrderIntent.IntentSlot-time",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "at what time would you like to schedule your food order?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.1492880162469.1383306652241",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "okay. and what is the quantity that you want to order?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.1201643730418.378245265682",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "at what time would you like your booking to be ?"
                        },
                        {
                            "type": "PlainText",
                            "value": "at what time would you like to book a cab for?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.1201643730418.1455008076377",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "when would you like your cab booking to be"
                        },
                        {
                            "type": "PlainText",
                            "value": "when would you like your booking to be?"
                        },
                        {
                            "type": "PlainText",
                            "value": "when would you like your cab to be booked for?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.1446900248282.1034572689561",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "At which restaurant would you like to book the table, Cascade, Chama Gaucha or the oriental blossom?"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent.111141204155",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "would you like to confirm your request for {Number} {Toiletries}"
                        },
                        {
                            "type": "PlainText",
                            "value": "would you like to place an order for {Number} {Toiletries}"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent.800837118355",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "would you like to confirm your request for bell boy?"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent.48608787701",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "would you like to confirm your table booking"
                        },
                        {
                            "type": "PlainText",
                            "value": "would like to confirm your table booking for {Number} ?"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent.1448742181414",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "would you like to place a request for connect to front desk?"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent.889293772264",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "would you like to confirm the request for laundry services?"
                        },
                        {
                            "type": "PlainText",
                            "value": "would you like to place a laundry request?"
                        },
                        {
                            "type": "PlainText",
                            "value": "would you like to place a request for laundry services?"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent.881023804387",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "would you like to confirm your complaint regarding {Complaints}"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent.328632889962",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "would you like to confirm your valet request?"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent.696790494114",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "would you like to confirm your cab booking request?"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent.627931296603",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "would you like to confirm your wake up call request on {Date} at {Time} ?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.75540223438.1431178814880",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "Sure. when would you like to have your wake up call?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.75540223438.1174920170145",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "and at what time?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.803941735269.1406019987826",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "okay how many luggage bags do you have?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.724436750957.193217565017",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "Would you like to have a second wake up call reminder, as well?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.950778260332.1063987566731",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "and would you like to have tea, or coffee, at that time as well?"
                        }
                    ]
                },
                {
                    "id": "Confirm.Intent.124148096016",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "gvjhvj?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.623034417450.1011341634024",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "at what time would you like to book the cab?"
                        }
                    ]
                },
                {
                    "id": "Elicit.Slot.623034417450.714983784085",
                    "variations": [
                        {
                            "type": "PlainText",
                            "value": "and on which day?"
                        }
                    ]
                }
            ]
        },
        "version": "9"
    }
    let slots = Model['interactionModel']['languageModel']['types'];
    slots.push(modelValues.types);
    console.log(Model['interactionModel']['languageModel']['types'])
    var json = JSON.stringify(Model);
    fs.writeFileSync('/tmp/en-US.json', json);
}