'use strict';

var AWS = require("aws-sdk");
var fs = require('fs');
const shell = require('shelljs');

AWS.config.update({
    region: "us-east-1",
    endpoint: "dynamodb.us-east-1.amazonaws.com	",
    "secretAccessKey": "NXt3qlQOW0kbPFT/Iani0pYyN2gVbkBcT37WXHAd",
    "accessKeyId": "AKIAUDHBUE6USG6RB736"
});

var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = function (event, context, callback) {
    const events = JSON.parse(event.body);

    let params = {
        TableName: events.hotelId,
        Key: {
            "ItemType": "AlexaSkill",
            "ItemName": "InvocationName"
        }
    }

    docClient.get(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            let invocationName = data.Item.invocationName;

            let queryParams = {
                TableName: events.hotelId,
                KeyConditionExpression: "#itemtype = :slot",
                ExpressionAttributeNames: {
                    "#itemtype": "ItemType"
                },
                ExpressionAttributeValues: {
                    ":slot": "Slots"
                }
            };

            docClient.query(queryParams, async function (err, data) {
                if (err) {
                    console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
                } else {
                    console.log("Query succeeded.");
                    let interactionJSON = {};
                    interactionJSON.invocationName = invocationName;
                    let typeArray = [];
                    data.Items.forEach(function (item) {
                        let values = item.values;
                        let obj = {
                            values,
                            name: item.ItemName
                        }
                        typeArray.push(obj);
                    });
                    console.log('typeArray', typeArray);
                    //generate the JSON file
                    interactionJSON.types = typeArray;
                    const InteractionJS = await generateInteractionJson(interactionJSON);
                    console.log("Inte", InteractionJS);

                    if (InteractionJS == true) {
                        console.log('Interaction JSON Generated');
                        let Interaction = shell.exec('cd scripts; node ./genInteractionModel.js').code;
                        if (Interaction === 0) {
                            console.log('Interaction Model Generated');
                            console.log('dir', __dirname);
                            console.log('filename', __filename);
                            //if (shell.exec('cd tmp; mv en-US.json ../Hotel/models/en-US.json').code === 0) {
                                //     console.log('Interaction Model Moved to skill folder');
                                //     if (shell.exec('cd Hotel; ask deploy --force').code === 0) {
                                //         console.log('Skill Deployed');
                                //         let Result = {
                                //             Success: true,
                                //             Message: "Build Completed"

                                //         }
                                //         callback(null, { "body": JSON.stringify(Result) });
                                //     } else {
                                //         let Result = {
                                //             Success: false,
                                //             Message: "Build Failed"
                                //         }
                                //         callback(null, { "body": JSON.stringify(Result) });
                                //     }
                                // } else {
                                //     let Result = {
                                //         Success: false,
                                //         Message: "Move en-US json failed"
                                //     }
                                //     callback(null, { "body": JSON.stringify(Result) });
                            //}
                        } else {
                            let Result = {
                                Success: false,
                                Message: "Interaction Model Generation Failed"
                            }
                            callback(null, { "body": JSON.stringify(Result) });
                        }
                    } else {
                        let Result = {
                            Success: false,
                            Message: "Interaction JSON Generation Failed"
                        }
                        callback(null, { "body": JSON.stringify(Result) });
                    }
                }
            });
        }
    });
}

async function generateInteractionJson(interactionJSON) {
    console.log('here');
    var json = JSON.stringify(interactionJSON);
    fs.writeFileSync('/tmp/interaction.json', json);
    return true;
}