'use strict';

var AWS = require("aws-sdk");

AWS.config.update({
    region: "us-east-1",
    endpoint: "dynamodb.us-east-1.amazonaws.com	",
    "secretAccessKey": "NXt3qlQOW0kbPFT/Iani0pYyN2gVbkBcT37WXHAd",
    "accessKeyId": "AKIAUDHBUE6USG6RB736"
});

var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = function (event, context, callback) {

    const events = JSON.parse(event.body);
    let params = {
        TableName: events.hotelId,
        Key: {
            "ItemType": "UserCreds",
            "ItemName": events.emailId
        }
    };

    docClient.get(params, checkCurrentPassword);
    function checkCurrentPassword(err, data) {
        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
            let Result = {
                Success: false,
                Error: { Message: "Database query error" }
            }
            callback(null, { "body": JSON.stringify(Result) });

        } else {

            if (data.Item.Password === events.currentPassword) {
                let params1 = {
                    TableName: events.hotelId,
                    Key: {
                        "ItemType": "UserCreds",
                        "ItemName": events.userName
                    },
                    UpdateExpression: "set Password = :newPassword",
                    ConditionExpression: "Password = :currentPassword",
                    ExpressionAttributeValues: {
                        ":newPassword": events.newPassword,
                        ":currentPassword": events.currentPassword,

                    },
                    ReturnValues: "UPDATED_NEW"
                };

                docClient.update(params1, updateUserPassword);

                function updateUserPassword(err1, data1) {
                    if (err1) {
                        console.error("Unable to query. Error:", JSON.stringify(err1, null, 2));
                        let Result = {
                            Success: false,
                            Error: { Message: "Database query error" }
                        }
                        callback(null, { "body": JSON.stringify(Result) });


                    } else {
                        let Result = { Success: true, Result: { Message: "Update successful" } };
                        callback(null, { "body": JSON.stringify(Result) });
                    }
                }
            } else {
                let Result = {
                    Success: false,
                    Error: { Message: "Password does not match" }
                }
                callback(null, { "body": JSON.stringify(Result) });

            }
        }
    }
}