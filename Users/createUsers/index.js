'use strict';

var AWS = require("aws-sdk");

AWS.config.update({
    region: "us-east-1",
    endpoint: "dynamodb.us-east-1.amazonaws.com	",
    "secretAccessKey": "NXt3qlQOW0kbPFT/Iani0pYyN2gVbkBcT37WXHAd",
    "accessKeyId": "AKIAUDHBUE6USG6RB736"
});

var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = function (event, context, callback) {

    const events = JSON.parse(event.body);

    let params = {
        TableName: events.hotelId,
        KeyConditionExpression: "#ItemType = :itemType",
        ExpressionAttributeNames: {
            "#ItemType": "ItemType"
        },
        ExpressionAttributeValues: {
            ":itemType": "UserCreds"
        }
    }

    docClient.query(params, validateUserName);

    function validateUserName(err, data) {

        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err1, null, 2));
            let Result = {
                Success: false,
                Error: { Message: "Database query error" }
            }
            callback(null, { "body": JSON.stringify(Result) });

        }
        else {

            if (data.Items.length !== 0) {

                console.log("not the first time");

                for (let i = 0; i < data.Items.length; i++) {

                    if (events.userName === data.Items[i].UserName) {

                        let Result = { Success: false, Error: { Message: "user name already exists" } }
                        callback(null, { "body": JSON.stringify(Result) });

                    } else {

                        let params1 = {
                            TableName: events.hotelId,
                            Item: {
                                "ItemType": "UserCreds",
                                "ItemName": events.emailId,
                                "Password": events.password,
                                "Permissions": events.permissions,
                                "adminflag": events.adminFlag,
                                "UserName": events.userName,
                                "FirstName": events.firstName,
                                "LastName": events.lastName,
                                "emailId": events.emailId,
                                "phoneNumber": events.phoneNumber
                            }
                        };

                        docClient.put(params1, createUserCreds);

                        function createUserCreds(err1, data1) {

                            if (err1) {
                                console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
                                let Result = {
                                    Success: false,
                                    Error: { Message: "Database query error" }
                                }
                                callback(null, { "body": JSON.stringify(Result) });

                            } else {
                                let user = {
                                    "Password": events.password,
                                    "Permissions": events.permissions,
                                    "adminflag": events.adminFlag,
                                    "UserName": events.userName,
                                    "FirstName": events.firstName,
                                    "LastName": events.lastName,
                                    "emailId": events.emailId,
                                    "phoneNumber": events.phoneNumber
                                }
                                let Result = {
                                    Success: true,
                                    Result: user
                                };
                                callback(null, { "body": JSON.stringify(Result) });

                            }

                        }
                    }
                }

            } else {
                console.log("first time");
                let params1 = {
                    TableName: events.hotelId,
                    Item: {
                        "ItemType": "UserCreds",
                        "ItemName": events.emailId,
                        "Password": events.password,
                        "Permissions": events.permissions,
                        "adminflag": events.adminFlag,
                        "UserName": events.userName,
                        "FirstName": events.firstName,
                        "LastName": events.lastName,
                        "emailId": events.emailId,
                        "phoneNumber": events.phoneNumber
                    }
                };

                docClient.put(params1, createUserCred);

                function createUserCred(err1, data1) {

                    if (err1) {
                        console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
                        let Result = {
                            Success: false,
                            Error: { Message: "Database query error" }
                        }
                        callback(null, { "body": JSON.stringify(Result) });

                    } else {
                        let user = {
                            "Password": events.password,
                            "Permissions": events.permissions,
                            "adminflag": events.adminFlag,
                            "UserName": events.userName,
                            "FirstName": events.firstName,
                            "LastName": events.lastName,
                            "emailId": events.emailId,
                            "phoneNumber": events.phoneNumber
                        }
                        let Result = {
                            Success: true,
                            Result: user
                        };
                        callback(null, { "body": JSON.stringify(Result) });
                    }
                }
            }
        }
    }
};