'use strict';
var Request = require("request");

var AWS = require("aws-sdk");

AWS.config.update({
    region: "us-east-1",
    endpoint: "dynamodb.us-east-1.amazonaws.com	",
    "secretAccessKey": "NXt3qlQOW0kbPFT/Iani0pYyN2gVbkBcT37WXHAd",
    "accessKeyId": "AKIAUDHBUE6USG6RB736"
});

var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {

    const events = JSON.parse(event.body);
    let params = {
        TableName: events.hotelId,
        KeyConditionExpression: "#ItemType = :itemType",
        ExpressionAttributeNames: {
            "#ItemType": "ItemType"
        },
        ExpressionAttributeValues: {
            ":itemType": "UserCreds"
        }
    }

    docClient.query(params, getAllUsers);

    function getAllUsers(err, data) {
        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
            let Result = {
                Success: false,
                Error: { Message: "Database query error" }
            }
            callback(null, { "body": JSON.stringify(Result) });
        } else {
            if (data.Items) {
                let users = [];
                for (let i = 0; i < data.Items.length; i++) {
                    users.push({ Permissions: data.Items[i].Permissions, UserName: data.Items[i].UserName, FirstName: data.Items[i].FirstName, LastName: data.Items[i].LastName })
                }
                let Result = {
                    Success: true,
                    Result: {
                        AllPermissions: [
                            {
                                title: "Housekeeping",
                                id: "AM,SR,LA"
                            },
                            {
                                title: "Front Desk",
                                id: "ECO,CO"
                            },
                            {
                                title: "Food Order",
                                id: "FO"
                            },
                            {
                                title: "Valet",
                                id: "VL"
                            },
                            {
                                title: "Spa Reservation",
                                id: "BS"
                            },
                            {
                                title: "Table Booking",
                                id: "BT"
                            }
                        ],
                        Users: users
                    }
                }
                callback(null, { "body": JSON.stringify(Result) });
            } else {
                let Result = {
                    Success: false,
                    Error: { Message: "No users in the database" }
                };
                callback(null, { "body": JSON.stringify(Result) });
            }
        }

    }
};