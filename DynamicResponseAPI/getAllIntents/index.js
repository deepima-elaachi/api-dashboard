'use strict';

exports.handler = function (event, context, callback) {

    let listOfIntents = {
        "AmenitiesIntent": ['amount', 'item'],
        "HousekeepingIntent": ['item'],
        "EngineeringIntent": ['item'],
        "LaundryIntent": []
    }
    let Result = {
        Success: true,
        data: {
            Intents: listOfIntents,
            currentTimestamp: new Date()
        }
    };

    callback(null, { "body": JSON.stringify(Result) });
}

//API end point  https://k6eqkws9i1.execute-api.us-east-1.amazonaws.com/dev/getAllIntents