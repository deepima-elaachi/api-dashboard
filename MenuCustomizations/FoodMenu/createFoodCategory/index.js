'use strict';

var AWS = require("aws-sdk");

AWS.config.update({
    region: "us-east-1",
    endpoint: "dynamodb.us-east-1.amazonaws.com	",
    "secretAccessKey": "NXt3qlQOW0kbPFT/Iani0pYyN2gVbkBcT37WXHAd",
    "accessKeyId": "AKIAUDHBUE6USG6RB736"
});

//change the access and secretAccesskey
var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = function (event, context, callback) {

    const events = JSON.parse(event.body);

    var params = {
        TableName: events.hotelId,
        Key: {
            "ItemType": "Slots",
            "ItemName": "ListOfFoodCategory"
        }
    };

    docClient.get(params, function (err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            let listOfCategory = data.Item.values;
            let flag = false;
            for (let i = 0; i < listOfCategory.length; i++) {
                if (listOfCategory[i].name.value.toLowerCase() === events.categoryName.toLowerCase()) {
                    flag = true;
                }
            }
            if (flag == true) {
                let Result = {
                    Success: false,
                    Message: "Category Name already exists!"
                }
                callback(null, { "body": JSON.stringify(Result) });
            } else {
                //add in db
                let newCategory = {
                    "name": {
                        "value": events.categoryName
                    }
                }
                listOfCategory.push(newCategory);
                var updateParams = {
                    TableName: events.hotelId,
                    Key: {
                        "ItemType": "Slots",
                        "ItemName": "ListOfFoodCategory"
                    },
                    UpdateExpression: "set values = :r",
                    ExpressionAttributeValues: {
                        ":r": listOfCategory,
                    },
                    ReturnValues: "UPDATED_NEW"
                }
                console.log("Updating the item...");
                docClient.update(updateParams, function (err1, data1) {
                    if (err) {
                        console.error("Unable to update item. Error JSON:", JSON.stringify(err1, null, 2));
                    } else {
                        console.log("UpdateItem succeeded:", JSON.stringify(data1, null, 2));
                        let Result = {
                            Success: true,
                            Message: "Category Name added!",
                        }
                        callback(null, { "body": JSON.stringify(Result) });
                    }
                });
            }
        }
    });
}

//  https://ew7h7mj294.execute-api.us-east-1.amazonaws.com/dev/createMenu