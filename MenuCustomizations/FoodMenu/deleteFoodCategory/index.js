'use strict';

var AWS = require("aws-sdk");

AWS.config.update({
    region: "us-east-1",
    endpoint: "dynamodb.us-east-1.amazonaws.com	",
    "secretAccessKey": "NXt3qlQOW0kbPFT/Iani0pYyN2gVbkBcT37WXHAd",
    "accessKeyId": "AKIAUDHBUE6USG6RB736"
});

var docClient = new AWS.DynamoDB.DocumentClient();

exports.handler = function (event, context, callback) {

    const events = JSON.parse(event.body);
    let params = {
        TableName: events.hotelId,
        Key: {
            "ItemType": "Slots",
            "ItemName": "ListOfFoodCategory"
        }
    };

    docClient.get(params, getFoodCategory);

    function getFoodCategory(err, data) {
        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
            let Result = {
                Success: false,
                Error: { Message: "Database query error" }
            }
            callback(null, { "body": JSON.stringify(Result) });

        } else {
            let listOfCategory = data.Item.values;
            let filteredCategory;
            for (let i = 0; i < listOfCategory.length; i++) {
                if (listOfCategory[i].name.value.toLowerCase() === events.categoryName.toLowerCase()) {
                    filteredCategory = listOfCategory.filter((item) => item.name.value.toLowerCase() !== events.categoryName.toLowerCase());
                    console.log('filteredCategory', filteredCategory);
                }
            }
            listOfCategory = filteredCategory;
            var updateParams = {
                TableName: events.hotelId,
                Key: {
                    "ItemType": "Slots",
                    "ItemName": "ListOfFoodCategory"
                },
                UpdateExpression: "set values = :r",
                ExpressionAttributeValues: {
                    ":r": listOfCategory,
                },
                ReturnValues: "UPDATED_NEW"
            };

            docClient.update(updateParams, function (err1, data1) {
                if (err) {
                    console.error("Unable to update item. Error JSON:", JSON.stringify(err1, null, 2));
                } else {
                    console.log("UpdateItem succeeded:", JSON.stringify(data1, null, 2));

                    let Result = {
                        Success: true,
                        Message: "Category Name deleted!",
                    }
                    callback(null, { "body": JSON.stringify(Result) });
                }
            });
            //delete food items
        }
    }
}