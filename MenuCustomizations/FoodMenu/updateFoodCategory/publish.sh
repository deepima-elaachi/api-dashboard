\rm -fr lambda_upload.zip
zip -r lambda_upload.zip index.js node_modules package.json
aws lambda update-function-code --function-name updateFoodCategory --zip-file fileb://lambda_upload.zip
